package com.theislandofthebahamas.response;

import com.theislandofthebahamas.datamodel.GeneralHotelInfo;

import java.util.List;

public class GeneralHotelResponse {
	String status = "";
	String message = "";
	List<GeneralHotelInfo> result;

	public List<GeneralHotelInfo> getResult() {
		return result;
	}

	public void setResult(List<GeneralHotelInfo> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
