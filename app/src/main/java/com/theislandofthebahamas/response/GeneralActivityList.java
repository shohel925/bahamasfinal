package com.theislandofthebahamas.response;

import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityTypesOfActivityInfo;
import com.theislandofthebahamas.datamodel.GeneralActvityThinksToKnowInfo;

import java.util.List;

public class GeneralActivityList {
	
	String id="";
	String name="";
	String image_url="";
	String image_title="";
	String description="";
	List<String> top_images;
	List<GeneralActivityIslandInfo> island_list;
	List<GeneralActvityThinksToKnowInfo> things_to_know;
	List<GeneralActivityTypesOfActivityInfo> types_of_activity;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getImage_title() {
		return image_title;
	}
	public void setImage_title(String image_title) {
		this.image_title = image_title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getTop_images() {
		return top_images;
	}
	public void setTop_images(List<String> top_images) {
		this.top_images = top_images;
	}
	public List<GeneralActivityIslandInfo> getIsland_list() {
		return island_list;
	}
	public void setIsland_list(List<GeneralActivityIslandInfo> island_list) {
		this.island_list = island_list;
	}
	public List<GeneralActvityThinksToKnowInfo> getThings_to_know() {
		return things_to_know;
	}
	public void setThings_to_know(List<GeneralActvityThinksToKnowInfo> things_to_know) {
		this.things_to_know = things_to_know;
	}
	public List<GeneralActivityTypesOfActivityInfo> getTypes_of_activity() {
		return types_of_activity;
	}
	public void setTypes_of_activity(List<GeneralActivityTypesOfActivityInfo> types_of_activity) {
		this.types_of_activity = types_of_activity;
	}
	
	
	

}
