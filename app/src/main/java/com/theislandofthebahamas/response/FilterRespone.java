package com.theislandofthebahamas.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilterRespone {
	@SerializedName("status")
	String status="";
	@SerializedName("message")
	String message="";
	@SerializedName("result")
	List<CategoryInfo> result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<CategoryInfo> getResult() {
		return result;
	}

	public void setResult(List<CategoryInfo> result) {
		this.result = result;
	}
	
	
	

}
