package com.theislandofthebahamas.response;

import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;

import java.util.List;

public class DivingDirectoryResponse {
	private String status="";
	private String message="";
	private List<DivingDirectoryInfo> result;
	
	
	
	public DivingDirectoryResponse() {
		
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<DivingDirectoryInfo> getResult() {
		return result;
	}
	public void setResult(List<DivingDirectoryInfo> result) {
		this.result = result;
	}
	
	

}
