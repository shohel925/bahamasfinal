package com.theislandofthebahamas;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.theislandofthebahamas.datamodel.TempIslandInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.util.ArrayList;
import java.util.List;

public class IslandListActivity extends Activity {
	Typeface tf ;
	Context con,context;
	private DatabaseHandler db;
	List<TempIslandInfo> tempList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_island_list);
		
		con = context = this;
		tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		TextView tvTitle = (TextView) findViewById(R.id.tv_appName_islandList);
		tvTitle.setTypeface(tf, Typeface.BOLD);
		db = new DatabaseHandler(context);
		tempList=new ArrayList<TempIslandInfo>();
		if(tempList!=null){
			tempList.clear();
		}
		tempList=AppConstant.tempIslandList(con);
		ListView lvIslandList = (ListView) findViewById(R.id.lv_islandList);
		lvIslandList.setAdapter(new IslandListAdapter());
		
		lvIslandList.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
									  int position, long id) {
				  TempIslandInfo data = tempList.get(position);
				  PersistData.setStringData(con, AppConstant.islandID,data.getId());
				  StartActivity.toActivity(context, TransportActivity.class);
			  }
			}); 
		
		
	}
	public void btBackOnClick(View v)
	{
		this.finish();
	}
	
	
	private class IslandListAdapter extends BaseAdapter {

		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tempList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tempList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_island_list_item, parent, false);
			}
			TempIslandInfo query=tempList.get(position);
			TextView txtIslandName = (TextView) convertView.findViewById(R.id.island_row_iteamName);

			txtIslandName.setText(query.getName());
			txtIslandName.setTextColor(Color.parseColor(query.getColor()));
			txtIslandName.setTypeface(tf);

			return convertView;
		}
		
	}
}
