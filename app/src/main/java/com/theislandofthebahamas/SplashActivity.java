package com.theislandofthebahamas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.datamodel.SecondSubcategoryInfo;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;
import com.theislandofthebahamas.datamodel.TransportInfo;
import com.theislandofthebahamas.datamodel.TransportIslandInfo;
import com.theislandofthebahamas.datamodel.TransportResult;
import com.theislandofthebahamas.datamodel.TransportTableData;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.CategoryInfo;
import com.theislandofthebahamas.response.DivingDirectoryResponse;
import com.theislandofthebahamas.response.FilterRespone;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.response.GeneralActivityResponse;
import com.theislandofthebahamas.response.GeneralHotelResponse;
import com.theislandofthebahamas.response.IslandListRespone;
import com.theislandofthebahamas.response.TransportResponse;
import com.theislandofthebahamas.util.AAPBDHttpClient;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.KeyHashManager;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

public class SplashActivity extends Activity {

	Handler handler = new Handler();
	Context con;
	IslandListRespone mIslandListRespone;
	GeneralActivityResponse mGeneralActivityResponse;
	DatabaseHandler db;
	ProgressBar progressbarSplash;
	LinearLayout progressLayout;
	Timer timer;
	TimerTask timerTask;
	ImageView imageviewOrginal;
	long timecurrent,previousTime;
	String time="";
	RelativeLayout linearMain;
	int heightPixel,widhtPixel,newHeight=3;
	long timerPreviousTime=0;
	ImageView imageGray;
	Typeface tf2;
	TextView textviewDownload,textviewPowerBy;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		con=this;
		db=new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(this, "Splash Screen", "");
		AnalyticsTracker.sendEventData(this, "Splash Screen", "", "");
		tf2 = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Demi.otf");
		Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		textviewDownload=(TextView)findViewById(R.id.textviewDownload);
		textviewDownload.setTypeface(tf2);
		textviewPowerBy=(TextView)findViewById(R.id.textviewPowerBy);
		textviewPowerBy.setTypeface(tf);
		imageviewOrginal=(ImageView)findViewById(R.id.imageviewOrginal);
		progressLayout=(LinearLayout)findViewById(R.id.progressLayout);
		progressbarSplash=(ProgressBar)findViewById(R.id.progressbarSplash);
		linearMain=(RelativeLayout)findViewById(R.id.linearMain);
		imageGray=(ImageView)findViewById(R.id.imageGray);
		final float scale = getResources().getDisplayMetrics().density;
		heightPixel = (int) (200 * scale + 0.5f);
		widhtPixel=heightPixel;
		KeyHashManager.getKeyHash(con);
		time = "" + System.currentTimeMillis();
		timerPreviousTime=System.currentTimeMillis();
		
		if (!PersistData.getStringData(con, AppConstant.firstHead).equalsIgnoreCase("Yes")) {
			startTimer();
			getIslandData(AllURL.getAllIslandUrl());
			PersistData.setStringData(con, AppConstant.previousTime, time);


		} else {
			timecurrent = Long.parseLong(time);
			previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

			if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
				if (NetInfo.isOnline(con)) {
						startTimer();
						PersistData.setStringData(con, AppConstant.previousTime, time);
						getIslandData(AllURL.getAllIslandUrl());

					}else{
					progressLayout.setVisibility(View.INVISIBLE);
					imageGray.setVisibility(View.GONE);
						handler.postDelayed(new Runnable() {

							@Override
							public void run() {
								StartActivity.toActivity(con,MainActivity.class);
								finish();
							}
						}, 2000);

					}


			}else{
				progressLayout.setVisibility(View.INVISIBLE);
				imageGray.setVisibility(View.GONE);
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						StartActivity.toActivity(con,MainActivity.class);
						finish();
					}
				}, 2000);

			}
		}
		
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		stoptimertask();
	}

	public void startTimer() {
		//set a new Timer
		timer = new Timer();
		timerTask = new TimerTask() {
			public void run() {

				//use a handler to run a toast that shows the current timestamp
				handler.post(new Runnable() {
					public void run() {
						time=""+System.currentTimeMillis();
						timecurrent=Long.parseLong(time);
						if(timecurrent-timerPreviousTime>28000){
							Log.e("Call",">>> Stop");
							imageGray.setVisibility(View.GONE);
							stoptimertask();
							handler.removeCallbacks(timerTask);
						}else{
						Log.e("Call",">>> timer");
						int finalHeight=widhtPixel-newHeight;
						Log.e("finalHeight",">>"+finalHeight);
						RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(widhtPixel, finalHeight);
						linearMain.setLayoutParams(rel_btn);
						newHeight=newHeight+3;
						}
					}
				});
			}
		};
		timer.schedule(timerTask, 150, 150); //
	}

	public void stoptimertask() {
		//stop the timer, if it's not already null
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	private void getIslandData(final String url) {
		// TODO Auto-generated method stub
		 if (!NetInfo.isOnline(con)) {
				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
					return;
				}
		 
		 	Log.e("URL : ", url);

//	        busyNow = new BusyDialog(con, true,false);
//	        busyNow.show();
				progressLayout.setVisibility(View.VISIBLE);
				progressbarSplash.setVisibility(View.VISIBLE);
		        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
		        	
		        	String response="";
					
					@Override
					public void run() {
						try {
							response= AAPBDHttpClient.post(url).body();
						}
						catch (Exception e) {
							// TODO: handle exception
							 Log.e("MYAPP", "exception", e);
//							 if(busyNow!=null){
//								 busyNow.dismis();
//							 }
						}

						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
//								if(busyNow!=null){
//									 busyNow.dismis();
//								 }
								try {
				                    Log.e("Response", ">>" + new String(response));
				                    if (!TextUtils.isEmpty(new String(response))) {
				    					Gson g = new Gson();
				    					mIslandListRespone = g.fromJson(new String(response),IslandListRespone.class);
				    					
				    					if (mIslandListRespone.getStatus().equalsIgnoreCase("true")) {
											
											int islandNo=1;
											for(IslandList ilist:mIslandListRespone.getResult()){
												if(!db.ifIslandExist(ilist.getId())){
													ilist.setIslandNo(islandNo);
													db.addIsland(ilist);
													for(int i=0; i<ilist.getDiscover_more_images().size(); i++){
														db.addIslandImage(ilist.getId(), ilist.getDiscover_more_images().get(i), (i+1), "discover_more_images");
														
													}
													for(int i=0; i<ilist.getIsland_details().size(); i++){
														ilist.getIsland_details().get(i).setIslandId(ilist.getId());
														ilist.getIsland_details().get(i).setSerialid(""+(i+1));
														db.addIslandDetails(ilist.getIsland_details().get(i));
														
													}
												}
												islandNo++;
											}
											progressbarSplash.setProgress(10);
											getGeneralActivityData(AllURL.getGeneralActivityUrl());
											
										} else {
											AlertMessage.showMessage(con, getString(R.string.app_name), mIslandListRespone.getMessage() + "");

										}
				                    }


				                } catch (final Exception e) {

				                    e.printStackTrace();
				                }
								
								
							}
						});
					}
				});
		
		
	}
	
	 
	 private void getGeneralActivityData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

//		        busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
							}

							runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
//									if(busyNow!=null){
//										 busyNow.dismis();
//									 }
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					mGeneralActivityResponse = g.fromJson(new String(response),GeneralActivityResponse.class);
					    					
					    					if (mGeneralActivityResponse.getStatus().equalsIgnoreCase("true")) {

					    						int skip=0;
					    						
					    						for(GeneralActivityList ilist:mGeneralActivityResponse.getResult()){
					    							
					    							
													if(!db.ifGeneralActivityExist(ilist.getId())){
														db.addGeneralActivity(ilist);
														int tempSize=ilist.getTop_images().size();
														
														int j=1;
														
														for(int i=skip; i<ilist.getTop_images().size(); i++){
															
																db.addIslandImage(ilist.getId(), ilist.getTop_images().get(i), j, "General_activity");
																j++;
													}
														skip=ilist.getTop_images().size();
														for(int i=0; i<ilist.getIsland_list().size(); i++){
															ilist.getIsland_list().get(i).setActivityId(ilist.getId());
															db.addGeneralActivityFeatureIsland(ilist.getIsland_list().get(i));
															
														}
														for(int i=0; i<ilist.getThings_to_know().size(); i++){
															ilist.getThings_to_know().get(i).setActivityId(ilist.getId());
															db.addThinksToKnow(ilist.getThings_to_know().get(i));
															
														}
														for(int i=0; i<ilist.getTypes_of_activity().size(); i++){
															ilist.getTypes_of_activity().get(i).setActivityId(ilist.getId());
															db.addTypesOfActivity(ilist.getTypes_of_activity().get(i));
															
														}
//														
													}
													

													
												}
												progressbarSplash.setProgress(30);
												getHotelData(AllURL.getHotelListUrl());
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralActivityResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}

	private void getHotelData(final String url) {
		// TODO Auto-generated method stub
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
			return;
		}

		Log.e("URL : ", url);

//		       final BusyDialog busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();
//		progressHotel.setVisibility(View.VISIBLE);

		Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {

			String response="";

			@Override
			public void run() {
				try {
					response=AAPBDHttpClient.post(url).body();
				}
				catch (Exception e) {
					// TODO: handle exception
					Log.e("MYAPP", "exception", e);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
//					progressHotel.setVisibility(View.GONE);
				}

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
//									if(busyNow!=null){
//										 busyNow.dismis();
//									 }
//						progressHotel.setVisibility(View.GONE);
						try {
							Log.e("Response", ">>" + new String(response));
							if (!TextUtils.isEmpty(new String(response))) {
								Gson g = new Gson();
								final GeneralHotelResponse mGeneralHotelResponse = g.fromJson(new String(response),GeneralHotelResponse.class);

								if (mGeneralHotelResponse.getStatus().equalsIgnoreCase("true")) {
//									PersistData.setBooleanData(con, AppConstant.isHotelHead, true);
									Log.e("Server hotel Size", ">>"+mGeneralHotelResponse.getResult().size());

									for(int i=0;i<mGeneralHotelResponse.getResult().size(); i++){
										if(!TextUtils.isEmpty(mGeneralHotelResponse.getResult().get(i).getId())){
											if(!db.ifHotelExist(mGeneralHotelResponse.getResult().get(i).getId())){
												db.addGeneralHotel(mGeneralHotelResponse.getResult().get(i));
											}
										}
									}
									progressbarSplash.setProgress(45);
									getFilterData(AllURL.getFilterApiUrl());

								} else {
									AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralHotelResponse.getMessage() + "");

								}
							}


						} catch (final Exception e) {

							e.printStackTrace();
						}


					}
				});
			}
		});


	}

	private void getFilterData(final String url) {
		// TODO Auto-generated method stub
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
			return;
		}

		Log.e("URL : ", url);
//		        final BusyDialog busyNow = new BusyDialog(con, true,false);
		Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {

			String response="";
			String status="",message="";

			@Override
			public void run() {
				try {
					response=AAPBDHttpClient.post(url).body();
				}
				catch (Exception e) {
					// TODO: handle exception
					Log.e("MYAPP", "exception", e);
//							 if(busyNow!=null){
//								 busyNow.dismis();
//							 }
				}

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
//								if(busyNow!=null){
//									 busyNow.dismis();
//								 }
						Log.e("response",">>"+response);
//						PersistData.setStringData(con, AppConstant.directoryFirstHeat, "Yes");
						Gson gson = new Gson();
						FilterRespone mFilterRespone=gson.fromJson(response, FilterRespone.class);
						if(mFilterRespone.getStatus().equalsIgnoreCase("true")){
							for(CategoryInfo cInfo:mFilterRespone.getResult()){
								if(!db.ifCategoryExist(cInfo.getId())){
									db.addCategory(cInfo.getId(), "", cInfo.getName(),"1");
									for(SubcategoryInfo sInfo:cInfo.getSubcategorylist()){
										db.addCategory(sInfo.getId(),cInfo.getId(),sInfo.getSubcategoryname(),"1");
										if(sInfo.getSubcategorylist()!=null){
											for(SecondSubcategoryInfo ssInfo:sInfo.getSubcategorylist()){
												db.addCategory(ssInfo.getId(), cInfo.getId(), ssInfo.getSubcategoryname(),"0");
											}
										}
									}
								}
							}
							progressbarSplash.setProgress(60);
							getTransportData(AllURL.getTransportApiUrl());

						}else{
							AlertMessage.showMessage(con, getString(R.string.app_name), mFilterRespone.getMessage());
						}



					}
				});
			}
		});


	}

	protected void getDivingDirectoryData(final String url)
	{
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
			return;
		}

		Log.e("URL : ", url);
//		busyNow = new BusyDialog(con, true,false);
//		busyNow.show();

		Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			String response="";
			DivingDirectoryResponse mDivingDirectoryResponse;
			@Override
			public void run() {

             /* call API and Do background task.*/
				try {
					response= AAPBDHttpClient.post(url).body();
					Log.e("Response", ">>" + new String(response));
				} catch (Exception e) {
					// TODO: handle exception
				}

				if (!TextUtils.isEmpty(new String(response))) {

					try
					{

						Log.e("Response", ">>" + new String(response));
						Gson g = new Gson();

						mDivingDirectoryResponse = g.fromJson(new String(response),DivingDirectoryResponse.class);


					}
					catch (final Exception e)
					{

						e.printStackTrace();
					}

	                /* Back to main thread/UI*/

					runOnUiThread(new Runnable() {
						@Override
						public void run() {
	                        /* Update your UI*/
//							if (busyNow != null) {
//								busyNow.dismis();
//							}

							if (mDivingDirectoryResponse.getStatus().equalsIgnoreCase("true")) {

								for(DivingDirectoryInfo dlist:mDivingDirectoryResponse.getResult()){
									if(!db.ifDivingDirExist(dlist.getId())){
										db.addDivingDir(dlist);
										for(String activityId:dlist.getActivity_ids()){
											if(!db.ifCategoryDirectoryExist(dlist.getId(),activityId)){
												db.addCategoryDirectory(dlist.getId(), activityId);
											}
										}
									}
								}
								progressbarSplash.setProgress(100);
								progressbarSplash.setVisibility(View.GONE);
								StartActivity.toActivity(con,MainActivity.class);
								finish();

							}
							else {
								AlertMessage.showMessage(con, getString(R.string.app_name), mDivingDirectoryResponse.getMessage() + "");
							}


						}
					});
				}

			}
		});

	}

	private void getTransportData(final String url) {
		// TODO Auto-generated method stub
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
			return;
		}

		Log.e("URL : ", url);
//		final BusyDialog busyNow = new BusyDialog(con, true,false);
//		busyNow.show();

		Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			String response="";
			@Override
			public void run() {

	                /* call API and Do background task.*/
				try {
					response= AAPBDHttpClient.post(url).body();
					Log.e("Response", ">>" + new String(response));
				} catch (Exception e) {
					// TODO: handle exception
				}

				if (!TextUtils.isEmpty(new String(response))) {
					Gson g = new Gson();

					TransportResponse mTransportResponse = g.fromJson(new String(response),TransportResponse.class);

					if (mTransportResponse.getStatus().equalsIgnoreCase("true"))
					{
						PersistData.setStringData(con, AppConstant.firstHead, "Yes");
//						PersistData.setStringData(con, AppConstant.transportFirstHeat, "Yes");
						TransportResult result = mTransportResponse.getResult();
						db.deleteAllTransportInfo();


						//clearing database

						for(TransportIslandInfo til:result.getHtga())
						{
							String isLandId = til.getIslandid();
							List<TransportInfo> transportInfoList=til.getTransportlist();
							for(TransportInfo ti:transportInfoList)
							{
								String trInfo = ti.getTransportInfo();
								String trType = ti.getTransportType();
								//Log.d("data sending to db","htga "+isLandId+" "+trType+" "+trInfo);
								TransportTableData ttd = new TransportTableData("htga", isLandId, trType, trInfo);
								db.addTransportData(ttd);

							}
						}
						for(TransportIslandInfo til:result.getHtgh())
						{
							String isLandId = til.getIslandid();
							List<TransportInfo> transportInfoList=til.getTransportlist();
							for(TransportInfo ti:transportInfoList)
							{
								String trInfo = ti.getTransportInfo();
								String trType = ti.getTransportType();
								//Log.d("data sending to db","htgh "+isLandId+" "+trType+" "+trInfo);
								TransportTableData ttd = new TransportTableData("htgh", isLandId, trType, trInfo);
								db.addTransportData(ttd);

							}
						}
					}

		                /* Back to main thread/UI*/

					runOnUiThread(new Runnable() {
						@Override
						public void run() {
		                        /* Update your UI*/
//							if (busyNow != null) {
//								busyNow.dismis();
//							}
							progressbarSplash.setProgress(75);

							getDivingDirectoryData(AllURL.getActivityDirectoryUrl());
						}
					});
				}

			}
		});


	}
}
