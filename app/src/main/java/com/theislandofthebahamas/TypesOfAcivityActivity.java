package com.theislandofthebahamas;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.theislandofthebahamas.adapter.TypesOfActivityAdapter;
import com.theislandofthebahamas.datamodel.GeneralActivityTypesOfActivityInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;

import java.util.Vector;

public class TypesOfAcivityActivity extends Activity {

	DatabaseHandler db;
	Context context, con;
	Vector<GeneralActivityTypesOfActivityInfo> tempImageList;
	ViewPager viewpagerGeneralDetails;
	LinearLayout indicator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.thinks_to_know);
		getActionBar().hide();
		context = con = this;
		db = new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(this,AppConstant.mGeneralActivityList.getName()+" Type Of Activity", "");
		AnalyticsTracker.sendEventData(this,AppConstant.mGeneralActivityList.getName()+" Type Of Activity" , "", "");
		if (tempImageList != null) {
			tempImageList.removeAllElements();
		}
		// =========== get all image from DB =================
		tempImageList = AppConstant.typeOfActivityList;
		viewpagerGeneralDetails = (ViewPager) findViewById(R.id.viewpagerThinks);
		indicator = (LinearLayout) findViewById(R.id.linearIndicator);
		viewpagerGeneralDetails.setAdapter(new TypesOfActivityAdapter(con, tempImageList));
		viewpagerGeneralDetails.setCurrentItem(0);
		controlDots(0);

		viewpagerGeneralDetails.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				// TODO Auto-generated method stub

				controlDots(pos);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	protected void controlDots(int newPageNumber) {

		try {
			indicator.removeAllViews(); // simple linear layout

			final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

			android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

			layoutParams.setMargins(7, 2, 0, 2);

			final ImageView[] b = new ImageView[tempImageList.size()];

			// Log.e("Shop size ", ">>>" + db.getAllShoppingName().size());

			for (int i1 = 0; i1 < tempImageList.size(); i1++) {

				b[i1] = new ImageView(con);

				b[i1].setId(1000 + i1);

				if (newPageNumber == i1) {

					b[i1].setBackgroundResource(R.drawable.things_to_know_06);

				}
				else {

					b[i1].setBackgroundResource(R.drawable.things_to_know_05);

				}

				b[i1].setLayoutParams(layoutParams);

				indicator.addView(b[i1]);

			}

		}
		catch (final Exception e) {

		}

	}

	public void closeGeneralActivityDetail(View v) {
		finish();

	}

}
