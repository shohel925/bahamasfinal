package com.theislandofthebahamas.datamodel;

import java.util.ArrayList;

public class MoreInformationDM {

	private int moreInfoId;
	private String moreInfoName;
	private ArrayList<MoreInfoDetailDM> moreInfoDetailList;
	private int moreInfoPicId;
	public int getMoreInfoId() {
		return moreInfoId;
	}
	public void setMoreInfoId(int moreInfoId) {
		this.moreInfoId = moreInfoId;
	}
	public String getMoreInfoName() {
		return moreInfoName;
	}
	public void setMoreInfoName(String moreInfoName) {
		this.moreInfoName = moreInfoName;
	}
	public ArrayList<MoreInfoDetailDM> getMoreInfoDetailList() {
		return moreInfoDetailList;
	}
	public void setMoreInfoDetailList(ArrayList<MoreInfoDetailDM> moreInfoDetailList) {
		this.moreInfoDetailList = moreInfoDetailList;
	}
	public int getMoreInfoPicId() {
		return moreInfoPicId;
	}
	public void setMoreInfoPicId(int moreInfoPicId) {
		this.moreInfoPicId = moreInfoPicId;
	}
	
}
