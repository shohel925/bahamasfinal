package com.theislandofthebahamas.datamodel;

import java.util.List;

public class IslandList {
	String id="";
	String name="";
	String image_url="";
	String logo="";
	String description="";
	String discover_more="";
	String discover_more_main_image="";
	int islandNo =0;
	
	

	public int getIslandNo() {
		return islandNo;
	}
	public void setIslandNo(int islandNo) {
		this.islandNo = islandNo;
	}
	List<String> discover_more_images;
	List<IslandDetailsList> island_details;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDiscover_more() {
		return discover_more;
	}
	public void setDiscover_more(String discover_more) {
		this.discover_more = discover_more;
	}
	public String getDiscover_more_main_image() {
		return discover_more_main_image;
	}
	public void setDiscover_more_main_image(String discover_more_main_image) {
		this.discover_more_main_image = discover_more_main_image;
	}
	public List<String> getDiscover_more_images() {
		return discover_more_images;
	}
	public void setDiscover_more_images(List<String> discover_more_images) {
		this.discover_more_images = discover_more_images;
	}
	public List<IslandDetailsList> getIsland_details() {
		return island_details;
	}
	public void setIsland_details(List<IslandDetailsList> island_details) {
		this.island_details = island_details;
	}
	

}
