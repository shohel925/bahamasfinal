package com.theislandofthebahamas.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class SubcategoryInfo {
	@SerializedName("id")
	String id = "";
	@SerializedName("subcategoryname")
	String subcategoryname = "";
	@SerializedName("parentId")
	String parentId = "";
	@SerializedName("subcategorylist")
	List<SecondSubcategoryInfo> subcategorylist ;
	private String finalSub="";
	public List<SecondSubcategoryInfo> getSubcategorylist() {
		return subcategorylist;
	}

	public void setSubcategorylist(List<SecondSubcategoryInfo> subcategorylist) {
		this.subcategorylist = subcategorylist;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubcategoryname() {
		return subcategoryname;
	}

	public void setSubcategoryname(String subcategoryname) {
		this.subcategoryname = subcategoryname;
	}

	public String getFinalSub() {
		return finalSub;
	}

	public void setFinalSub(String finalSub) {
		this.finalSub = finalSub;
	}
}
