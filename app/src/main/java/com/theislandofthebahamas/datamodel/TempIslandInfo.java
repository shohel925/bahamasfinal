package com.theislandofthebahamas.datamodel;

import android.graphics.drawable.Drawable;

public class TempIslandInfo {
	private String id;
	private String name;
	private Drawable image;
	private String color;
	private double lat;
	private double lng;
	private float zoomLabel;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Drawable getImage() {
		return image;
	}

	public void setImage(Drawable image) {
		this.image = image;
	}

	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public float getZoomLabel() {
		return zoomLabel;
	}

	public void setZoomLabel(float zoomLabel) {
		this.zoomLabel = zoomLabel;
	}
}
