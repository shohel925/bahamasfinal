
package com.theislandofthebahamas.datamodel;

import java.util.ArrayList;
import java.util.List;


public class TransportResult {
	
	private List<TransportIslandInfo> htgh=new ArrayList<TransportIslandInfo>();
	private List<TransportIslandInfo> htga=new ArrayList<TransportIslandInfo>();
	
	
	public List<TransportIslandInfo> getHtgh() {
		return htgh;
	}
	public void setHtgh(List<TransportIslandInfo> htgh) {
		this.htgh = htgh;
	}
	public List<TransportIslandInfo> getHtga() {
		return htga;
	}
	public void setHtga(List<TransportIslandInfo> htga) {
		this.htga = htga;
	}
	
	

}
