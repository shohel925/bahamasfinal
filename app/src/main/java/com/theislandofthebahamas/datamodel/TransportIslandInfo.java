/**
 * 
 */
package com.theislandofthebahamas.datamodel;

import java.util.List;

public class TransportIslandInfo {
	private String islandid;
	private String shoartcode;
	private List<TransportInfo> transportlist;
	
	public String getIslandid() {
		return islandid;
	}
	public void setIslandid(String islandid) {
		this.islandid = islandid;
	}
	public String getShoartcode() {
		return shoartcode;
	}
	public void setShoartcode(String shoartcode) {
		this.shoartcode = shoartcode;
	}
	public List<TransportInfo> getTransportlist() {
		return transportlist;
	}
	public void setTransportlist(List<TransportInfo> transportlist) {
		this.transportlist = transportlist;
	}
	
	

}
