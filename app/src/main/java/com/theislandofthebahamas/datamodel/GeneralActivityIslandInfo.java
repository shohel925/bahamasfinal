package com.theislandofthebahamas.datamodel;

public class GeneralActivityIslandInfo {
	
	String island_id="";
	String island_name="";
	String island_activity_id="";
	String island_activity_title="";
	String island_activity_image="";
	String island_activity_description="";
	String activityId="";
	
	
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getIsland_id() {
		return island_id;
	}
	public void setIsland_id(String island_id) {
		this.island_id = island_id;
	}
	public String getIsland_name() {
		return island_name;
	}
	public void setIsland_name(String island_name) {
		this.island_name = island_name;
	}
	public String getIsland_activity_id() {
		return island_activity_id;
	}
	public void setIsland_activity_id(String island_activity_id) {
		this.island_activity_id = island_activity_id;
	}
	public String getIsland_activity_title() {
		return island_activity_title;
	}
	public void setIsland_activity_title(String island_activity_title) {
		this.island_activity_title = island_activity_title;
	}
	public String getIsland_activity_image() {
		return island_activity_image;
	}
	public void setIsland_activity_image(String island_activity_image) {
		this.island_activity_image = island_activity_image;
	}
	public String getIsland_activity_description() {
		return island_activity_description;
	}
	public void setIsland_activity_description(String island_activity_description) {
		this.island_activity_description = island_activity_description;
	}
	
	

}
