package com.theislandofthebahamas.datamodel;

import com.google.gson.annotations.SerializedName;


public class SecondSubcategoryInfo {
	@SerializedName("id")
	String id="";
	@SerializedName("subcategoryname")
	String subcategoryname="";
	 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubcategoryname() {
		return subcategoryname;
	}
	public void setSubcategoryname(String subcategoryname) {
		this.subcategoryname = subcategoryname;
	}
	
	
}
