package com.theislandofthebahamas.datamodel;

public class CategoryDirectoryInfo {
	private String directoryId="";
	private String categoryId="";
	public String getDirectoryId() {
		return directoryId;
	}
	public void setDirectoryId(String directoryId) {
		this.directoryId = directoryId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	
}
