package com.theislandofthebahamas.datamodel;

public class MoreInfoDetailDM {

	private int moreInfoDetailId;
	private String moreInfoTitle;
	private String moreInfoDescription;
	public int getMoreInfoDetailId() {
		return moreInfoDetailId;
	}
	public void setMoreInfoDetailId(int moreInfoDetailId) {
		this.moreInfoDetailId = moreInfoDetailId;
	}
	public String getMoreInfoTitle() {
		return moreInfoTitle;
	}
	public void setMoreInfoTitle(String moreInfoTitle) {
		this.moreInfoTitle = moreInfoTitle;
	}
	public String getMoreInfoDescription() {
		return moreInfoDescription;
	}
	public void setMoreInfoDescription(String moreInfoDescription) {
		this.moreInfoDescription = moreInfoDescription;
	}
	
}
