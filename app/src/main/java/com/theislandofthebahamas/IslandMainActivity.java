package com.theislandofthebahamas;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.theislandofthebahamas.datamodel.ActivityDM;
import com.theislandofthebahamas.datamodel.EventDM;
import com.theislandofthebahamas.datamodel.HotelDM;
import com.theislandofthebahamas.datamodel.IslandDM;
import com.theislandofthebahamas.datamodel.MoreInfoDetailDM;
import com.theislandofthebahamas.datamodel.MoreInformationDM;
import com.theislandofthebahamas.fragment.IslandActivitiesFragment;
import com.theislandofthebahamas.fragment.IslandEventsFragment;
import com.theislandofthebahamas.fragment.IslandHomeFragment;
import com.theislandofthebahamas.fragment.IslandHotelsFragment;
import com.theislandofthebahamas.fragment.IslandMoreFragment;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.StartActivity;

import java.util.ArrayList;
import java.util.Locale;

public class IslandMainActivity extends Activity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the sections. We use a {@link FragmentPagerAdapter} derivative,
	 * which will keep every loaded fragment in memory. If this becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	Context context, con;

	// Fragment list
	IslandHomeFragment islandHomeFragment;
	IslandActivitiesFragment mIslandActivitiesFragment;
	IslandHotelsFragment mIslandHotelsFragment;
	IslandEventsFragment mIslandEventsFragment;
	IslandMoreFragment mIslandMoreFragment;
	TextView btnOpenHome,btnOpenActivities,btnOpenHotels,btnOpenEvents,btnOpenMore;

	LatLng[] latLngs = new LatLng[20];
	Typeface tf2;
	public ImageView islandMap;

	
	 public static IslandMainActivity instance;

	 public static IslandMainActivity getInstance() {
	  return instance;
	 }
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = con = this;
		instance=this;
		updateCustomActionBar();

		btnOpenHome = (TextView) findViewById(R.id.btnOpenHome);
		btnOpenActivities = (TextView) findViewById(R.id.btnOpenActivities);
		btnOpenHotels = (TextView) findViewById(R.id.btnOpenHotels);
		btnOpenEvents = (TextView) findViewById(R.id.btnOpenEvents);
		btnOpenMore = (TextView) findViewById(R.id.btnOpenMore);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.islandMainPager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOnPageChangeListener(mainPageChangeListener);

		// Demo
		btnOpenHome.setBackgroundResource(R.drawable.events_yellow_bg);
		btnOpenActivities.setBackgroundResource(0x0);
		btnOpenHotels.setBackgroundResource(0x0);
		btnOpenEvents.setBackgroundResource(0x0);
		btnOpenMore.setBackgroundResource(0x0);
		tf2 = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Demi.otf");
		btnOpenHome.setTypeface(tf2);
		btnOpenActivities.setTypeface(tf2);
		btnOpenHotels.setTypeface(tf2);
		btnOpenEvents.setTypeface(tf2);
		btnOpenMore.setTypeface(tf2);
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.main, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle action bar item clicks here. The action bar will // automatically handle clicks
	 * on the Home/Up button, so long // as you specify a parent activity in AndroidManifest.xml. int id = item.getItemId(); if (id == R.id.action_settings) {
	 * return true; } return super.onOptionsItemSelected(item); }
	 */

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).

			switch (position) {
			case 0:
				if (islandHomeFragment == null) {
					islandHomeFragment = new IslandHomeFragment();
				}
				return islandHomeFragment;
			case 1:
				if (mIslandActivitiesFragment == null) {
					mIslandActivitiesFragment = new IslandActivitiesFragment();
				}
				return mIslandActivitiesFragment;
			case 2:
				if (mIslandHotelsFragment == null) {
					mIslandHotelsFragment = new IslandHotelsFragment();
				}
				return mIslandHotelsFragment;
			case 3:
				if (mIslandEventsFragment == null) {
					mIslandEventsFragment = new IslandEventsFragment();
				}
				return mIslandEventsFragment;
			case 4:
				if (mIslandMoreFragment == null) {
					mIslandMoreFragment = new IslandMoreFragment();
				}
				return mIslandMoreFragment;

			default:
				return null;
			}
			// return PlaceholderFragment.newInstance(position + 1);

		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 5;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return "a";
			case 4:
				return "b";
			}
			return null;
		}
	}

	public void openMainHome(View v) {
		mViewPager.setCurrentItem(0);
	}

	public void openMainActivities(View v) {
		mViewPager.setCurrentItem(1);
	}

	public void openMainHotels(View v) {
		mViewPager.setCurrentItem(2);
	}

	public void openMainEvents(View v) {
		mViewPager.setCurrentItem(3);
	}

	public void openMainMore(View v) {
		mViewPager.setCurrentItem(4);
	}

	private OnPageChangeListener mainPageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageSelected(int position) {
			// TODO Auto-generated method stub
			switch (position) {
			case 0:
				btnOpenHome.setBackgroundResource(R.drawable.events_yellow_bg);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				AnalyticsTracker.sendTrackData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Home", "");
				AnalyticsTracker.sendEventData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Home" , "", "");
				break;
			case 1:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(R.drawable.events_red_bg);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				AnalyticsTracker.sendTrackData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Activities", "");
				AnalyticsTracker.sendEventData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Activities" , "", "");
				break;
			case 2:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(R.drawable.events_green_bg);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				AnalyticsTracker.sendTrackData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Hotels", "");
				AnalyticsTracker.sendEventData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Hotels" , "", "");
				break;
			case 3:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(R.drawable.events_bg);
				btnOpenMore.setBackgroundResource(0x0);
				AnalyticsTracker.sendTrackData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Events", "");
				AnalyticsTracker.sendEventData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" Events" , "", "");
				break;
			case 4:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(R.drawable.events_brick_bg);
				AnalyticsTracker.sendTrackData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" More", "");
				AnalyticsTracker.sendEventData(IslandMainActivity.this, AppConstant.mIslandList.getName()+" More" , "", "");
				break;
			default:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				break;
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}
	};

	private void updateCustomActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.ab_island_main_home);
		ImageView islandBack = (ImageView) actionBar.getCustomView().findViewById(R.id.islandBack);
		islandMap = (ImageView) actionBar.getCustomView().findViewById(R.id.islandMap);
		TextView islandTv = (TextView) actionBar.getCustomView().findViewById(R.id.islandTv);
		islandTv.setText(AppConstant.mIslandList.getName());
		islandTv.setTextColor(Color.parseColor(AppConstant.tmpImgInfo.getColor()));
		islandTv.setTypeface(tf2);
		ImageView islandSearch = (ImageView) actionBar.getCustomView().findViewById(R.id.islandSearch);

		islandBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		islandSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con,SearchActivity.class);
			}
		});
		

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mViewPager.getCurrentItem() == 0) {
			super.onBackPressed();
		}
		else {
			mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
		}
	}
}
