package com.theislandofthebahamas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.GMapV2GetRouteDirection;
import com.theislandofthebahamas.util.GPSTracker;

import java.util.ArrayList;
import java.util.Vector;

public class DirectionActivity extends Activity {
	
	Context con;
	private GoogleMap googleMap;
	LinearLayout infoView;
	///========== for Draw A path =====================
	LatLng fromPosition;
	LatLng toPosition;
	GMapV2GetRouteDirection v2GetRouteDirection;
	org.w3c.dom.Document document;
	public static DirectionActivity instance;
	public static DirectionActivity getInstance(){
		return instance;
	}
	GPSTracker gps;
	Typeface gotham;
	//// test
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.direction);
		con = this;
		instance=this;
		gps=new GPSTracker(con);
		updateCustomActionbar();
		 initUI();
	}

	
	private void initUI() {
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		MapFragment fm = (MapFragment) getFragmentManager().findFragmentById(R.id.directionMap);

		// Getting GoogleMap object from the fragment
		googleMap = fm.getMap();

	}

	
	private void loadRoutData(Double currentLat, Double currentLng) {
		
		fromPosition = new LatLng(currentLat,currentLng);
		toPosition = new LatLng(Double.parseDouble(AppConstant.mDivingDirectoryInfo.getLat()), Double.parseDouble(AppConstant.mDivingDirectoryInfo.getLon()));
		v2GetRouteDirection = new GMapV2GetRouteDirection();
		GetRouteTask getRoute = new GetRouteTask();
		getRoute.execute();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (gps.canGetLocation()) {
			loadRoutData(gps.getLatitude(),gps.getLongitude());
		} else {
			gps.showSettingsAlert();
		}
	}

	private void updateCustomActionbar(){
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.ab_hotel_detail);

		TextView btnCloseHotelDetail = (TextView)actionBar.getCustomView().findViewById(R.id.btnCloseHotelDetail);
		Button btnEditHotelInfo = (Button)actionBar.getCustomView().findViewById(R.id.btnEditHotelInfo);
		TextView txtHotelName = (TextView)actionBar.getCustomView().findViewById(R.id.txtHotelName);
		btnEditHotelInfo.setVisibility(View.GONE);

		try{
			txtHotelName.setText("Direction");
			txtHotelName.setTypeface(gotham);
			btnEditHotelInfo.setTypeface(gotham);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		btnCloseHotelDetail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

//		btnEditHotelInfo.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				AppConstant.islandOrDirectoryId=AppConstant.mDivingDirectoryInfo.getId();
//				AppConstant.objectType="directory";
//				StartActivity.toActivity(con, ReportActivity.class);
//			}
//		});

	}
	 BusyDialog busyDialog;
	
	private class GetRouteTask extends AsyncTask<String, Void, String> {

		String response = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			busyDialog = new BusyDialog(con, true, false);
			busyDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {
			document = v2GetRouteDirection.getDocument(fromPosition, toPosition, GMapV2GetRouteDirection.MODE_DRIVING);
			response = "Success";
			return response;
		}
		MarkerOptions marker1;
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			googleMap.clear();
			if (response.equalsIgnoreCase("Success")) {
				ArrayList<LatLng> directionPoint = v2GetRouteDirection.getDirection(document);
				PolylineOptions rectLine = new PolylineOptions().width(7).color(Color.BLUE);

				rectLine.add(fromPosition);
				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
				}
				rectLine.add(toPosition);
				// Adding route on the map
				googleMap.addPolyline(rectLine);
				/*
				 * markerOptions.position(toPosition);
				 * googleMap.addMarker(markerOptions);
				 */

				final MarkerOptions marker = new MarkerOptions().position(fromPosition).title("Your are here");
				marker.draggable(true);
				marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

				googleMap.addMarker(marker);
				marker1 = new MarkerOptions().position(toPosition).title(AppConstant.mDivingDirectoryInfo.getName());
				marker1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
				Marker m=googleMap.addMarker(marker1);
				//m.showInfoWindow();

				Vector<LatLng> allp = new Vector<LatLng>();
				allp.addElement(fromPosition);
				allp.addElement(toPosition);
				
				LatLngBounds.Builder builder = new LatLngBounds.Builder();
				builder.include(fromPosition);
				builder.include(toPosition);
				
				LatLngBounds bounds = builder.build();
				int padding = 100;
				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//				cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
				googleMap.animateCamera(cu);
				

			}
			if (busyDialog != null) {
				busyDialog.dismis();
			}
			// showRout = false;
		}

	}


	
}
