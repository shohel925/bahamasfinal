package com.theislandofthebahamas;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.adapter.GeneralDetailsPagerAdapter;
import com.theislandofthebahamas.adapter.WeddingDetailsPagerAdapter;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.IslandImageInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.DivingDirectoryResponse;
import com.theislandofthebahamas.util.AAPBDHttpClient;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.StartActivity;

import java.util.Vector;
import java.util.concurrent.Executors;

public class GeneralActivityDetailActivity extends Activity {

	TextView txtGeneralActivityName;
	TextView txtGeneralActivityDesc;
	Button btnGeneralActivityDirectory;
	ViewGroup llGeneralActivityFeatureIslandHolder;
	DatabaseHandler db;	
	Context context, con;
	Vector<IslandImageInfo> tempImageList;
	ViewPager viewpagerGeneralDetails;
	LinearLayout indicator;
	Vector<GeneralActivityIslandInfo> tempIslandInfo;
	
	DivingDirectoryResponse mDivingDirectoryResponse;
	BusyDialog busyNow;
	TextView btnCloseActivityDetail,txtFeatureIsland;
	Typeface medium,gotham,demiBold;
	Button btnGeneralActivityThingsToKnow,typesOfActivity;
	private Vector<Integer>weddingList;
	int size=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_general_activity_detail);
		context = con = this;
		db=new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(this, "Bahamas Activities", "");
		AnalyticsTracker.sendEventData(this, "Bahamas Activities" , "", "");
		if(tempImageList!=null){
			tempImageList.removeAllElements();
		}
		if(tempIslandInfo!=null){
			tempIslandInfo.removeAllElements();
		}
		btnCloseActivityDetail=(TextView)findViewById(R.id.btnCloseActivityDetail);
		btnCloseActivityDetail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		weddingList= new Vector<>();
		if (weddingList!=null){
			weddingList.removeAllElements();
		}
		weddingList.add(R.drawable.abaco);
		weddingList.add(R.drawable.gbi);
		weddingList.add(R.drawable.npi);
		typesOfActivity=(Button)findViewById(R.id.typesOfActivity);
		btnGeneralActivityThingsToKnow=(Button)findViewById(R.id.btnGeneralActivityThingsToKnow);
		txtFeatureIsland=(TextView)findViewById(R.id.txtFeatureIsland);
		txtGeneralActivityName = (TextView)findViewById(R.id.txtGeneralActivityName);
		txtGeneralActivityDesc = (TextView) findViewById(R.id.txtGeneralActivityDesc);
		btnGeneralActivityDirectory = (Button) findViewById(R.id.btnGeneralActivityDirectory);
		llGeneralActivityFeatureIslandHolder = (ViewGroup) findViewById(R.id.llGeneralActivityFeatureIslandHolder);
		tempIslandInfo=db.getFeactureListFromActivityId(AppConstant.mGeneralActivityList.getId());
		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		typesOfActivity=(Button)findViewById(R.id.typesOfActivity);
		txtGeneralActivityName.setTypeface(demiBold);
		typesOfActivity.setText(getString(R.string.typesOf)+" "+AppConstant.mGeneralActivityList.getName().toUpperCase());
				
				try {
					//=========== get all image from DB =================
					tempImageList=db.getAllIslandImage(AppConstant.mGeneralActivityList.getId(), "General_activity");
					viewpagerGeneralDetails=(ViewPager)findViewById(R.id.viewpagerGeneralDetails);
					indicator=(LinearLayout)findViewById(R.id.linearIndicator);
					if (AppConstant.mGeneralActivityList.getId().equalsIgnoreCase("37")){
						size=3;
						viewpagerGeneralDetails.setAdapter(new WeddingDetailsPagerAdapter(con, weddingList));
					}else{
						size=tempImageList.size();
						viewpagerGeneralDetails.setAdapter(new GeneralDetailsPagerAdapter(con, tempImageList));
					}
					viewpagerGeneralDetails.setCurrentItem(0);
					controlDots(0);
					
					viewpagerGeneralDetails.setOnPageChangeListener(new OnPageChangeListener() {

						@Override
						public void onPageSelected(int pos) {
							// TODO Auto-generated method stub

							controlDots(pos);
						}

						@Override
						public void onPageScrolled(int arg0, float arg1, int arg2) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onPageScrollStateChanged(int arg0) {
							// TODO Auto-generated method stub

						}
					});
					txtGeneralActivityName.setText(AppConstant.mGeneralActivityList.getImage_title() + " ");
					txtGeneralActivityDesc.setText(AppConstant.mGeneralActivityList.getDescription().replaceAll("\\<[^>]*>",""));
					btnGeneralActivityDirectory.setText(AppConstant.mGeneralActivityList.getName().toUpperCase() + " " + getResources().getString(R.string.btn_directory));
					txtGeneralActivityName.setTypeface(gotham);
					txtGeneralActivityDesc.setTypeface(medium);
					btnGeneralActivityDirectory.setTypeface(demiBold);
					txtFeatureIsland.setTypeface(gotham);
					typesOfActivity.setTypeface(demiBold);
					btnGeneralActivityThingsToKnow.setTypeface(demiBold);
					loadFeatureIslands();
					
				}
				catch (Exception e) {
					// TODO: handle exception
				}
		
	}
	protected void controlDots(int newPageNumber) {

		try {
			indicator.removeAllViews(); // simple linear layout

			final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

			android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

			layoutParams.setMargins(7, 2, 0, 2);

			final ImageView[] b = new ImageView[size];

			// Log.e("Shop size ", ">>>" + db.getAllShoppingName().size());

			for (int i1 = 0; i1 < size; i1++) {

				b[i1] = new ImageView(con);

				b[i1].setId(1000 + i1);

				if (newPageNumber == i1) {

					b[i1].setBackgroundResource(R.drawable.things_to_know_06);

				} else {

					b[i1].setBackgroundResource(R.drawable.things_to_know_05);

				}

				b[i1].setLayoutParams(layoutParams);

				indicator.addView(b[i1]);

			}

		} catch (final Exception e) {

		}

	}
	View customView;
	int i;
	private void loadFeatureIslands() {
		// TODO Auto-generated method stub
		llGeneralActivityFeatureIslandHolder.removeAllViews();
		for(i = 0; i< tempIslandInfo.size(); i++){
			customView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_feature_list, llGeneralActivityFeatureIslandHolder, false);
			ImageView imgActivityPic = (ImageView) customView.findViewById(R.id.imgActivityPic);
			TextView txtActivityName = (TextView) customView.findViewById(R.id.txtActivityName);
			txtActivityName.setTypeface(gotham);
			Picasso.with(context).load(tempIslandInfo.get(i).getIsland_activity_image()).into(imgActivityPic);
			txtActivityName.setText(tempIslandInfo.get(i).getIsland_name().toUpperCase());
			customView.setTag(i);
			customView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					Log.e("ilandId", ">>"+tempIslandInfo.get(i).getIsland_id());
					Object tag =v.getTag();
			        if(tag!=null) {      
			            int position = (Integer)tag;
			            AppConstant.mGeneralActivityIslandInfo= tempIslandInfo.get(position);
			            StartActivity.toActivity(con, FeatureIslandDetailActivity.class);
			         } 
				}
			});
			
			llGeneralActivityFeatureIslandHolder.addView(customView);
			
		}
		
		
	}

	public void closeGeneralActivityDetail(View v){
		finish();
	}
	public void typeOfActivity(View v){
		AppConstant.typeOfActivityList=db.getTypesOfActivityListFromActivityId(AppConstant.mGeneralActivityList.getId());
		if(AppConstant.typeOfActivityList.size()>0){
			StartActivity.toActivity(con, TypesOfAcivityActivity.class);
		}else{
			Toast.makeText(context, "No data for Found", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	public void openEchoTourismDirectory(View v){
		AppConstant.isFromIsland=false;
		StartActivity.toActivity(con, DivingDirListActivity.class);
	}
	
	public void openThingsToKnow(View v){
		
		AppConstant.generalThinksToKnow=db.getThinksToKnowListFromActivityId(AppConstant.mGeneralActivityList.getId());
		if(AppConstant.generalThinksToKnow.size()>0){
			StartActivity.toActivity(con, ThinksToKnowActivity.class);
		}else{
			Toast.makeText(context, "No data for Found", Toast.LENGTH_SHORT).show();
		}
		
	}


 protected void getDivingDirectoryData(final String url)
 {
	 if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
				return;
		}

     Log.e("URL : ", url);
     busyNow = new BusyDialog(con, true,false);
     busyNow.show();

     Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
         String response="";
         @Override
         public void run() {

             /* call API and Do background task.*/
         	try {
	         		response= AAPBDHttpClient.post(url).body();
		                Log.e("Response", ">>" + new String(response));
					} catch (Exception e) {
						// TODO: handle exception
				}

	             if (!TextUtils.isEmpty(new String(response))) {

	            	 try
	            	 {

	            		Log.e("Response", ">>" + new String(response));
						Gson g = new Gson();

						mDivingDirectoryResponse = g.fromJson(new String(response),DivingDirectoryResponse.class);


		            }
	            	catch (final Exception e)
	            	{

		               e.printStackTrace();
		            }

	                /* Back to main thread/UI*/

	                runOnUiThread(new Runnable() {
	                    @Override
	                    public void run() {
	                        /* Update your UI*/
	                    	if (busyNow != null) {
	    	                    busyNow.dismis();
	    	                }
	                    	if (mDivingDirectoryResponse.getStatus().equalsIgnoreCase("true")) {

								for(DivingDirectoryInfo dlist:mDivingDirectoryResponse.getResult()){
									if(!db.ifDivingDirExist(dlist.getId())){
										db.addDivingDir(dlist);
										for(String activityId:dlist.getActivity_ids()){
											db.addCategoryDirectory(dlist.getId(), activityId);
										}
									}
								}
//								for(CategoryDirectoryInfo cInfo:db.getCategoryDirectoryList()){
//									Log.e("CategoryId", ""+cInfo.getCategoryId());
//									Log.e("directoryId", ""+cInfo.getDirectoryId());
//
//								}

								StartActivity.toActivity(con, DivingDirListActivity.class);
							}
							else {
								AlertMessage.showMessage(con, getString(R.string.app_name), mDivingDirectoryResponse.getMessage() + "");
							}


	                    }
	                });
             }

         }
     });

 }

}
