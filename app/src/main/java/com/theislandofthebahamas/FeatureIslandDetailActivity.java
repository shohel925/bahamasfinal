package com.theislandofthebahamas;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.FeaturePropertyInfo;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.CategoryInfo;
import com.theislandofthebahamas.response.DivingDirectoryResponse;
import com.theislandofthebahamas.response.FeaturePropertyResponse;
import com.theislandofthebahamas.response.FilterRespone;
import com.theislandofthebahamas.util.AAPBDHttpClient;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.Helper;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Executors;

public class FeatureIslandDetailActivity extends Activity {

	TextView txtGeneralActivityName;
	TextView txtGeneralActivityDesc;
	Button btnGeneralActivityDirectory,typesOfActivity;
	ViewGroup llGeneralActivityFeatureIslandHolder;
	DatabaseHandler db;	
	Context context, con;
	ViewPager viewpagerGeneralDetails;
	LinearLayout indicator;
//	Vector<GeneralActivityIslandInfo> tempIslandInfo;
	ImageView imageviewFeature;
	DivingDirectoryResponse mDivingDirectoryResponse;
	BusyDialog busyNow;
	TextView btnCloseActivityDetail,textviewFeatuereProperty;
	Vector<FeaturePropertyInfo> tempFeatureProperyList;
	Typeface medium,gotham,demiBold;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.feature_island_detail);
		context = con = this;
		db=new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(this,AppConstant.mGeneralActivityList.getName()+" "+AppConstant.mGeneralActivityIslandInfo.getIsland_name(), "");
		AnalyticsTracker.sendEventData(this, AppConstant.mGeneralActivityList.getName()+" "+AppConstant.mGeneralActivityIslandInfo.getIsland_name() , "", "");
		btnCloseActivityDetail=(TextView)findViewById(R.id.btnCloseActivityDetail);
		btnCloseActivityDetail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		textviewFeatuereProperty=(TextView)findViewById(R.id.textviewFeatuereProperty);
		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		imageviewFeature=(ImageView)findViewById(R.id.imageviewFeature);
		Picasso.with(con).load(AppConstant.mGeneralActivityIslandInfo.getIsland_activity_image()).into(imageviewFeature);
		 		typesOfActivity=(Button)findViewById(R.id.typesOfActivity);
		 		typesOfActivity.setText(getString(R.string.typesOf)+" "+AppConstant.mGeneralActivityList.getName().toUpperCase());
				txtGeneralActivityName = (TextView)findViewById(R.id.txtGeneralActivityName);
				txtGeneralActivityDesc = (TextView) findViewById(R.id.txtGeneralActivityDesc);
				btnGeneralActivityDirectory = (Button) findViewById(R.id.btnGeneralActivityDirectory);
				llGeneralActivityFeatureIslandHolder = (ViewGroup) findViewById(R.id.llGeneralActivityFeatureIslandHolder);
			    txtGeneralActivityName.setText(AppConstant.mGeneralActivityIslandInfo.getIsland_activity_title() + " ");
				txtGeneralActivityDesc.setText(AppConstant.mGeneralActivityIslandInfo.getIsland_activity_description().replaceAll("\\<[^>]*>",""));
				//btnGeneralActivityDirectory.setText(AppConstant.mGeneralActivityList.getName() + " " + getResources().getString(R.string.btn_directory));
				txtGeneralActivityName.setTypeface(gotham);
				txtGeneralActivityDesc.setTypeface(medium);
				btnGeneralActivityDirectory.setTypeface(demiBold);
				textviewFeatuereProperty.setTypeface(demiBold);
					 if (!NetInfo.isOnline(con)) {
							tempFeatureProperyList=db.getAllFeatureProperty(AppConstant.mGeneralActivityList.getId(),AppConstant.mGeneralActivityIslandInfo.getIsland_id());
						 if(tempFeatureProperyList.size()>0){
							 textviewFeatuereProperty.setVisibility(View.VISIBLE);
							 loadFeatureProperty(tempFeatureProperyList);
						 }else{
							 textviewFeatuereProperty.setVisibility(View.GONE);
						 }

						}else {
							getIslandPropertyData(AllURL.getIslandProperty());
						}
					
	}
	private void getIslandPropertyData(final String url) {
		// TODO Auto-generated method stub
//		 if (!NetInfo.isOnline(con)) {
//				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
//					return;
//				}

		        Log.e("URL : ", url);
		        final BusyDialog busyNow = new BusyDialog(con, true,false);
		        busyNow.show();
		        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
		        	
		        	String response="";
					
					@Override
					public void run() {
						
						Map<String, String> params= new HashMap<String, String>();
						params.put("islandId", AppConstant.mGeneralActivityIslandInfo.getIsland_id());
						params.put("activityId", AppConstant.mGeneralActivityList.getId());
					
						try {
							response= AAPBDHttpClient.post(url).form(params).body();
						}
						catch (Exception e) {
							// TODO: handle exception
							 Log.e("MYAPP", "exception", e);
							 if(busyNow!=null){
								 busyNow.dismis();
							 }
						}

						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(busyNow!=null){
									 busyNow.dismis();
								 }
								Log.e("response",">>"+response);
								Gson g=new Gson();
								FeaturePropertyResponse mFeaturePropertyResponse=g.fromJson(response, FeaturePropertyResponse.class);
								if(mFeaturePropertyResponse.getStatus().equalsIgnoreCase("true")){
									
									for(FeaturePropertyInfo fInofo:mFeaturePropertyResponse.getResult()){
										if(!db.ifFeatureProperyExist(fInofo.getId())){
											db.addFeatureProperty(fInofo,AppConstant.mGeneralActivityList.getId(),AppConstant.mGeneralActivityIslandInfo.getIsland_id());
										}
									}
									tempFeatureProperyList=db.getAllFeatureProperty(AppConstant.mGeneralActivityList.getId(),AppConstant.mGeneralActivityIslandInfo.getIsland_id());
									if(tempFeatureProperyList.size()>0){
										textviewFeatuereProperty.setVisibility(View.VISIBLE);
										loadFeatureProperty(tempFeatureProperyList);
									}else{
										textviewFeatuereProperty.setVisibility(View.GONE);
									}
//									StartActivity.toActivity(con, DivingDirListActivity.class);
								}else{
//									AlertMessage.showMessage(con, getString(R.string.app_name), mFeaturePropertyResponse.getMessage());
									textviewFeatuereProperty.setVisibility(View.GONE);
								}
								
								
								
							}
						});
					}
				});
		
	}
	private void loadFeatureProperty(final Vector<FeaturePropertyInfo> temp) {
		// TODO Auto-generated method stub
		llGeneralActivityFeatureIslandHolder.removeAllViews();
		for(int i = 0; i< temp.size(); i++){
			View customView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_diving_dir_detail, llGeneralActivityFeatureIslandHolder, false);
			ImageView imgDivingDirPic = (ImageView) customView.findViewById(R.id.imgDivingDetaisRow);
			TextView txtDivingDirName = (TextView) customView.findViewById(R.id.tv_diving_dir_name);
			TextView txtDivingDirLocation = (TextView) customView.findViewById(R.id.tv_diving_dir_location);
			TextView txtDivingDirPhone = (TextView) customView.findViewById(R.id.tv_diving_dir_phone);
			txtDivingDirName.setTypeface(medium);
			txtDivingDirLocation.setTypeface(medium);
			txtDivingDirPhone.setTypeface(medium);
			Picasso.with(context).load(temp.get(i).getImage()).placeholder(R.drawable.temp).into(imgDivingDirPic);
			txtDivingDirName.setText(temp.get(i).getTitle());
			txtDivingDirPhone.setText(temp.get(i).getPhone());
			txtDivingDirLocation.setText(temp.get(i).getAddress().replaceAll("\\<[^>]*>",""));
			customView.setTag(i);
			customView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					Log.e("ilandId", ">>"+tempIslandInfo.get(i).getIsland_id());
					Object tag =v.getTag();
			        if(tag!=null) {      
			            int position = (Integer)tag;
			           final FeaturePropertyInfo query= temp.get(position);
			           if((query.getPhone().trim().equals("-"))||(query.getPhone().trim().equals("false"))){
							AlertMessage.showMessage(con, con.getString(R.string.app_name), "Phone number not available");
						}else{
							 Helper.CustomPhoneCall(con, query.getPhone());
						}
			          
			         } 
				}
			});
			
			llGeneralActivityFeatureIslandHolder.addView(customView);
			
		}
		
		
	}

	public void closeGeneralActivityDetail(View v){
		finish();
	}
	public void typeOfActivity(View v){
		AppConstant.typeOfActivityList=db.getTypesOfActivityListFromActivityId(AppConstant.mGeneralActivityList.getId());
		if(AppConstant.typeOfActivityList.size()>0){
			StartActivity.toActivity(con, TypesOfAcivityActivity.class);
		}else{
			Toast.makeText(context, "No data for Found", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	public void openEchoTourismDirectory(View v){
		AppConstant.isFromIsland=true;
		AppConstant.islandID=AppConstant.mGeneralActivityIslandInfo.getIsland_id();
		StartActivity.toActivity(con, DivingDirListActivity.class);
		
	}
	
	public void openThingsToKnow(View v){
		
		AppConstant.generalThinksToKnow=db.getThinksToKnowListFromActivityId(AppConstant.mGeneralActivityList.getId());
		if(AppConstant.generalThinksToKnow.size()>0){
			StartActivity.toActivity(con, ThinksToKnowActivity.class);
		}else{
			Toast.makeText(context, "No data for Found", Toast.LENGTH_SHORT).show();
		}
		
	}

	
 protected void getDivingDirectoryData(final String url)
 {
	 if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
				return;
		}

     Log.e("URL : ", url);
     busyNow = new BusyDialog(con, true,false);
     busyNow.show();	        
			        
     Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
         String response="";
         @Override
         public void run() {
             
             /* call API and Do background task.*/	
         	try {
	         		response= AAPBDHttpClient.post(url).body();		                
		                Log.e("Response", ">>" + new String(response));
					} catch (Exception e) {
						// TODO: handle exception
				}
             
	             if (!TextUtils.isEmpty(new String(response))) {
						
	            	 try 
	            	 {
		                    
	            		Log.e("Response", ">>" + new String(response));
						Gson g = new Gson();
						
						mDivingDirectoryResponse = g.fromJson(new String(response),DivingDirectoryResponse.class);
						
		                    
		            } 
	            	catch (final Exception e)
	            	{
	
		               e.printStackTrace();
		            }
            	 	
	                /* Back to main thread/UI*/
	                
	                runOnUiThread(new Runnable() {
	                    @Override
	                    public void run() {	                        
	                        /* Update your UI*/
	                    	if (busyNow != null) {
	    	                    busyNow.dismis();
	    	                }
	                    	if (mDivingDirectoryResponse.getStatus().equalsIgnoreCase("true")) {
								
								for(DivingDirectoryInfo dlist:mDivingDirectoryResponse.getResult()){
									if(!db.ifDivingDirExist(dlist.getId())){
										db.addDivingDir(dlist);
										for(String activityId:dlist.getActivity_ids()){
											db.addCategoryDirectory(dlist.getId(), activityId);
										}
									}
								}
//								for(CategoryDirectoryInfo cInfo:db.getCategoryDirectoryList()){
//									Log.e("CategoryId", ""+cInfo.getCategoryId());
//									Log.e("directoryId", ""+cInfo.getDirectoryId());
//									
//								}
								
								StartActivity.toActivity(con, DivingDirListActivity.class);
							} 
							else {
								AlertMessage.showMessage(con, getString(R.string.app_name), mDivingDirectoryResponse.getMessage() + "");
							}
	                    	
	                        
	                    }
	                });
             }
             
         }
     });
		
 }

}
