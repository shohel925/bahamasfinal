/**
 *
 */
package com.theislandofthebahamas.util;


import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

/**
 * @author Shohel
 *
 */
public class MyApplication extends Application {

//	public static final String PROPERTY_ID = "UA-63145393-1";
	public static final String PROPERTY_ID = "UA-75013706-1";
	private static MyApplication mInstance;

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
		// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
		// company.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			analytics.setLocalDispatchPeriod(10);
			/*
			 * Tracker t = (trackerId == TrackerName.APP_TRACKER) ?
			 * analytics.newTracker(PROPERTY_ID) : (trackerId ==
			 * TrackerName.GLOBAL_TRACKER) ?
			 * analytics.newTracker(R.xml.global_tracker) :
			 * analytics.newTracker(R.xml.ecommerce_tracker);
			 */

			Tracker t = null;
			if (trackerId == TrackerName.APP_TRACKER) {
				t = analytics.newTracker(PROPERTY_ID);
			}

			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}
}
