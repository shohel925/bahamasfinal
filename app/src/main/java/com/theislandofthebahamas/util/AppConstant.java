package com.theislandofthebahamas.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityTypesOfActivityInfo;
import com.theislandofthebahamas.datamodel.GeneralActvityThinksToKnowInfo;
import com.theislandofthebahamas.datamodel.GeneralEventInfo;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.datamodel.TempIslandInfo;
import com.theislandofthebahamas.response.GeneralActivityList;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

public class AppConstant {
	
	public  static String getDaysFromDateAndTime(String times){
		long time = Long.parseLong(times);
			time = 1367832568 * (long) 1000;
		    Date date = new Date(time);
		    SimpleDateFormat format = new SimpleDateFormat("EEEE");
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		    String result=format.format(date);
		    return result;
	}
	public static long checkHours(String startdate, String endDate){
		Date previousDate = new Date();
		previousDate.setTime((Long.parseLong(startdate))*1000);

		Date currentDate = new Date();
		currentDate .setTime((long)(Long.parseLong(endDate))*1000);

		//To calculate the days difference between two dates 
		long diffInhours = (long)( (currentDate.getTime() - previousDate.getTime()) 
		                 / (1000 * 60 * 60));
		 return diffInhours;
	}
	public static String dateConvert(String times){
		long time = Long.parseLong(times);
			time = time * (long) 1000;
		    Date date = new Date(time);
		    SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd");
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		    String result=format.format(date);
		    return result;
	}
	
	public static String dateConvertWithYear(String times){
		long time = Long.parseLong(times);
			time = time * (long) 1000;
		    Date date = new Date(time);
		    SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd yyyy");
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		    String result=format.format(date);
		    return result;
	}
	//=========== Convert Array to String ===============
	public static String strSeparator = ",";
	public static String convertArrayToString(List<String> array){
	    String str = "";
	    for (int i = 0;i<array.size(); i++) {
	        str = str+array.get(i);
	        // Do not append comma at the end of last element
	        if(i<array.size()-1){
	            str = str+strSeparator;
	        }
	    }
	    return str;
	}

	//Constants for Intent data passing
	public static final String SELECTED_EVENT_ID = "SelectedEventId";
	public static final String SELECTED_MORE_INFO_ID = "SelectedMoreInfoId";
	public static final String SELECTED_HOTEL_ID = "SelectedHotelId";
	public static final String SELECTED_GENERAL_ACTIVITY_ID = "SelectedGeneralActivityId";
	public static String firstHead = "firstHead";
	public static String previousTime = "previousTime";
	public static String Email = "testappuser3";
	public static String Password = "11111";
	public static String transportFirstHeat = "transportFirstHeat";

	public static String Token = "9cfef61d13e679d41acdafbc494f3dbe";
	public static String tempIsland = "";
	public static String islandName = "";
	//public static String islandColor = "#22222";
	public static GeneralActivityList mGeneralActivityList=null;
//	public static String activityId="";
	public	static Vector<GeneralActivityTypesOfActivityInfo> typeOfActivityList=null;
	public static Vector<GeneralActvityThinksToKnowInfo> generalThinksToKnow=null;
	public static GeneralHotelInfo mGeneralHotelInfo=null;
	public static GeneralEventInfo mGeneralEventInfo=null;
	public static String tempData="";
	public static String about_us="about_us";
	public static String government="government";
	public static String history="history";
	public static String holidays="holidays";
	public static String local_customs="local_customs";
	public static String language="language";
	public static String people="people";
	public static String proximity="proximity";
	public static String travel_tips="travel_tips";
	public static String more_discription="";
	public static String islandSerialNo="islandSerialNo";
	public static String isHotelHead="isHotelHead";
	public static String directoryFirstHeat="directoryFirstHeat";
	
	public static String islandID="islandID";
	public static String islandOrDirectoryId="";
	public static String objectType="";
	public static DivingDirectoryInfo mDivingDirectoryInfo=null;
	public static GeneralActivityIslandInfo mGeneralActivityIslandInfo;
	
	//=========== add island image ===================
	public static Vector<TempIslandInfo> tempIslandList(Context context){
		Vector<TempIslandInfo> tempList=new Vector<TempIslandInfo>();
		if(tempList!=null){
			tempList.clear();
		}
		TempIslandInfo tmp=new TempIslandInfo();
		tmp.setId("16");
		tmp.setColor("#EEB040");
		tmp.setName("The Abacos");
		tmp.setImage(getDrawableFormAsset(context,"a_16.jpg"));
		tmp.setLat(26.401884);
		tmp.setLng(-77.145749);
		tmp.setZoomLabel(7.0f);
		tempList.add(tmp);
		
		TempIslandInfo tmp2=new TempIslandInfo();
		tmp2.setId("25");
		tmp2.setColor("#4F95B8");
		tmp2.setName("Acklins & Crooked Island");
		tmp2.setImage(getDrawableFormAsset(context,"a_25.jpg"));
		tmp2.setLat(22.433333);
		tmp2.setLng(-73.966667);
		tmp2.setZoomLabel(8.0f);
		tempList.add(tmp2);
		
		TempIslandInfo tmp3=new TempIslandInfo();
		tmp3.setId("3");
		tmp3.setColor("#FFCF01");
		tmp3.setName("Andros");
		tmp3.setImage(getDrawableFormAsset(context,"a_3.jpg"));
		tmp3.setLat(24.724814);
		tmp3.setLng(-78.017850);
		tmp3.setZoomLabel(7.0f);
		tempList.add(tmp3);
		
		TempIslandInfo tmp4=new TempIslandInfo();
		tmp4.setId("17");
		tmp4.setColor("#EAA8C1");
		tmp4.setName("The Berry Islands");
		tmp4.setImage(getDrawableFormAsset(context,"a_17.jpg"));
		tmp4.setLat(25.6191171);
		tmp4.setLng(-77.995062);
		tmp4.setZoomLabel(10.0f);
		tempList.add(tmp4);
		
		TempIslandInfo tmp5=new TempIslandInfo();
		tmp5.setId("1");
		tmp5.setColor("#49A942");
		tmp5.setName("Bimini");
		tmp5.setImage(getDrawableFormAsset(context,"a_1.jpg"));
		tmp5.setLat(25.7332939);
		tmp5.setLng(-79.3079567);
		tmp5.setZoomLabel(12.0f);
		tempList.add(tmp5);
		
		TempIslandInfo tmp6=new TempIslandInfo();
		tmp6.setId("19");
		tmp6.setColor("#0194D3");
		tmp6.setName("Cat Island");
		tmp6.setImage(getDrawableFormAsset(context,"a_19.jpg"));
		tmp6.setLat(24.401788);
		tmp6.setLng(-75.5123);
		tmp6.setZoomLabel(8.0f);
		tempList.add(tmp6);
		
		TempIslandInfo tmp7=new TempIslandInfo();
		tmp7.setId("20");
		tmp7.setColor("#739DD2");
		tmp7.setName("Eleuthera & Harbour Island");
		tmp7.setImage(getDrawableFormAsset(context,"a_20.jpg"));
		tmp7.setLat(25.0856212);
		tmp7.setLng(-76.7996003);
		tmp7.setZoomLabel(8.0f);
		tempList.add(tmp7);
		
		TempIslandInfo tmp8=new TempIslandInfo();
		tmp8.setId("22");
		tmp8.setColor("#D15980");
		tmp8.setName("The Exumas");
		tmp8.setImage(getDrawableFormAsset(context,"a_22.jpg"));
		tmp8.setLat(24.074686);
		tmp8.setLng(-76.5439861);
		tmp8.setZoomLabel(6.0f);
		tempList.add(tmp8);
		
		TempIslandInfo tmp9=new TempIslandInfo();
		tmp9.setId("18");
		tmp9.setColor("#F58025");
		tmp9.setName("Grand Bahama Island");
		tmp9.setImage(getDrawableFormAsset(context,"a_18.jpg"));
		tmp9.setLat(26.6849551);
		tmp9.setLng(-78.7176982);
		tmp9.setZoomLabel(8.0f);
		tempList.add(tmp9);
		
		TempIslandInfo tmp10=new TempIslandInfo();
		tmp10.setId("28");
		tmp10.setColor("#66B360");
		tmp10.setName("Inagua");
		tmp10.setImage(getDrawableFormAsset(context,"a_28.jpg"));
		tmp10.setLat(21.024893);
		tmp10.setLng(-73.511174);
		tmp10.setZoomLabel(9.0f);
		tempList.add(tmp10);
		
		
		TempIslandInfo tmp11=new TempIslandInfo();
		tmp11.setId("23");
		tmp11.setColor("#2CAFA4");
		tmp11.setName("Long Island");
		tmp11.setImage(getDrawableFormAsset(context,"a_23.jpg"));
		tmp11.setLat(23.253258);
		tmp11.setLng(-75.4243137);
		tmp11.setZoomLabel(8.0f);
		tempList.add(tmp11);
		
		
		TempIslandInfo tmp12=new TempIslandInfo();
		tmp12.setId("27");
		tmp12.setColor("#BB8D0B");
		tmp12.setName("Mayaguana");
		tmp12.setImage(getDrawableFormAsset(context,"a_27.jpg"));
		tmp12.setLat(22.3759705);
		tmp12.setLng(-73.0787011);
		tmp12.setZoomLabel(9.0f);
		tempList.add(tmp12);
		
		
		TempIslandInfo tmp13=new TempIslandInfo();
		tmp13.setId("2");
		tmp13.setColor("#EE3524");
		tmp13.setName("Nassau & Paradise Island");
		tmp13.setImage(getDrawableFormAsset(context,"a_2.jpg"));
		tmp13.setLat(25.0709617);
		tmp13.setLng(-77.4754867);
		tmp13.setZoomLabel(10.0f);
		tempList.add(tmp13);
		
		TempIslandInfo tmp14=new TempIslandInfo();
		tmp14.setId("30");
		tmp14.setColor("#BC73B0");
		tmp14.setName("Ragged Island");
		tmp14.setImage(getDrawableFormAsset(context,"a_30.jpg"));
		tmp14.setLat(22.3204791);
		tmp14.setLng(-75.8668895);
		tmp14.setZoomLabel(9.0f);
		tempList.add(tmp14);
		
		
		TempIslandInfo tmp15=new TempIslandInfo();
		tmp15.setId("24");
		tmp15.setColor("#7874B6");
		tmp15.setName("Rum Cay");
		tmp15.setImage(getDrawableFormAsset(context,"a_24.jpg"));
		tmp15.setLat(23.6734933);
		tmp15.setLng(-74.9411444);
		tmp15.setZoomLabel(12.0f);
		tempList.add(tmp15);
		
		TempIslandInfo tmp16=new TempIslandInfo();
		tmp16.setId("31");
		tmp16.setColor("#026CB6");
		tmp16.setName("San Salvador");
		tmp16.setImage(getDrawableFormAsset(context,"a_31.jpg"));
		tmp16.setLat(24.012773);
		tmp16.setLng(-74.6176607);
		tmp16.setZoomLabel(10.0f);
		tempList.add(tmp16);
		return tempList;
	}

	public static List<Drawable> getActivityImage(Context context){
		List<Drawable> tmpIMageList=new ArrayList<>();
		if(tmpIMageList!=null){
			tmpIMageList.clear();
		}
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_beaches.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_boating.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_convention.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_dining.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_diving.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_ecoturism.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_family_fun.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_fishing.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_honeymoons.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_nightlife.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_pampering.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_private_flying.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_romantic_gateways.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_shoping.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_sightseeing.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_sports.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_water_sports.jpg"));
		tmpIMageList.add(getDrawableFormAsset(context,"img_home_activity_wedding.jpg"));
		return tmpIMageList;
	}

	private static Drawable getDrawableFormAsset(Context context, String imageName){
		Drawable d;
		try
		{
			// get input stream
			InputStream ims = context.getAssets().open(imageName);
			// load image as Drawable
			d = Drawable.createFromStream(ims, null);
			// set image to ImageVie
		}
		catch(IOException ex)
		{
			return null;
		}
		return  d;
	}
	
	public static final int IMAGE_SLIDE_ANIMATION_SPEED = 4;
	public static IslandList mIslandList=null;
	public static boolean isViewDirectoryOfHotel=false;
	public static  int islandPos=0;
	public static TempIslandInfo tmpImgInfo=null;
	public static boolean isFromIsland=false;
}
