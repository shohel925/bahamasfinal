/**
 * 
 */
package com.theislandofthebahamas.util;

import android.util.Log;

import com.theislandofthebahamas.datamodel.TransportDetailsDataModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;


public class ParseFromString {
	
	
	private String contentString;
	String[] textSplitResult ;
	private Document doc;
	private List<TransportDetailsDataModel> data;
	private static String name, address, fax, phone, webSite,email;
	
	
	public ParseFromString(String contentString) {
		super();
		this.contentString = contentString;
		doc = Jsoup.parse(contentString);
	}
	
	public List<TransportDetailsDataModel> getParseHtmlData(){
		
		data = new ArrayList<TransportDetailsDataModel>();
		
		for (Element pElement : doc.select("p"))
		{
			

			String stringHtml = pElement.html();
			textSplitResult = stringHtml.split("<br>");
			
			
			name = "";
			address = "";
			phone = "";
			email = "";
			fax = "";
			webSite = "";

			
			for(String s: textSplitResult)
			{
				
				//Log.e("exmaiming text",s);
				
				selectName(s);
				selectAddress(s);				
				selectPhone(s);
				selectFax(s);
				selectWebsite(s);
				selectEmail(s);
				
			}
			TransportDetailsDataModel dm = new TransportDetailsDataModel(name, address, phone, fax, email, webSite);
			data.add(dm);

		}
		
		return data;
		
	}
	
	
	private void selectAddress(String s) {
		Log.d("select Address","In Select Address method");
		if(s.contains("<em>")||s.contains("Phone:")||s.contains("Email:")||
				s.contains("Fax:")||s.contains("Website:")
				||s.contains("www")||s.contains("Toll Free:"))
			return;
		else
		{
			Log.d("else","In Select Address Else block");
			Document tempDoc = Jsoup.parse(s);
			address = address.concat(tempDoc.text());
		}
		
	}

	private void selectName(String s) {
		
		if(s.contains("<em>"))
		{
			Document tempDoc = Jsoup.parse(s);
			name = tempDoc.text();			
		}
			
		
	}
	
	/**
	 * Return the phone number if a string is provided
	 * and contains a pattern like %Phone::%
	 */
	private void selectPhone(String s) {
		
		if(s.contains("Phone:")||s.contains("Toll Free:"))
		{
			String temp = s.trim();
			phone = temp.replace("Phone:", "");
			phone =phone.trim();
			
		}
		
		
	}
	
	/**
	 * Return the Fax number if a string is provided
	 * and contains a pattern like %Fax%"
	 */
	private void selectFax(String s) {
		
		if(s.contains("Fax:"))
		{
			String temp = s.trim();
			fax = temp.replace("Fax:", "");
			fax =fax.trim();
		}
		
		
	}
	
	/**
	 * Return the WebSite url if a string is provided
	 * and contains a pattern like %Website%"
	 */
	private void selectWebsite(String s) {
		
		if(s.contains("Website:")||s.contains("www."))
		{
			Document tempDoc = Jsoup.parse(s);
			Elements tempElement = tempDoc.select("a");
			//select link
			//webSite = tempElement.attr("href");
			//select text
			webSite = tempElement.text(); 
		}
		
		
	}
	/**
	 * select the WebSite number if a string is provided
	 * and contains a pattern like %Email%"
	 */
	private void selectEmail(String s) {
		
		if(s.contains("Email:")&&!s.contains("www"))
		{
			Document tempDoc = Jsoup.parse(s);
			Elements tempElement = tempDoc.select("a");
			//select link
			//webSite = tempElement.attr("href");
			//select text
			email= tempElement.text(); 
		}
		
	}
	

}
