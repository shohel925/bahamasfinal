package com.theislandofthebahamas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.theislandofthebahamas.util.AppConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class EventDetailActivity extends Activity {

	TextView txtEventName;
	TextView txtEventFrom;
	TextView txtEventDetail,btnCloseEventDetail;
	Button btnCallEvnet,btnShareOnFb;
	
	Context context;
	List<String> tempPhone;
	Typeface medium,gotham,demiBold;
	ShareDialog shareDialog;
	CallbackManager callbackManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_detail);
		context = this;
				getActionBar().hide();
				btnCloseEventDetail=(TextView)findViewById(R.id.btnCloseEventDetail);
				txtEventName = (TextView) findViewById(R.id.txtEventName);
				txtEventFrom = (TextView) findViewById(R.id.txtEventFrom);
				txtEventDetail = (TextView) findViewById(R.id.txtEventDetail);
				btnCallEvnet = (Button)findViewById(R.id.btnCallEvnet);
				btnShareOnFb=(Button)findViewById(R.id.btnShareOnFb);
				medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
				gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
				demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
				txtEventName.setTypeface(gotham);
				txtEventFrom.setTypeface(medium);
				txtEventDetail.setTypeface(medium);
				btnCallEvnet.setTypeface(demiBold);
				btnShareOnFb.setTypeface(demiBold);
				if(AppConstant.mGeneralEventInfo.getYearly().trim().equalsIgnoreCase("true")){
					txtEventFrom.setText(AppConstant.getDaysFromDateAndTime(AppConstant.mGeneralEventInfo.getStartdate())+" / "+getString(R.string.Allyearround));
				}else{
					if(AppConstant.checkHours(AppConstant.mGeneralEventInfo.getStartdate(),AppConstant.mGeneralEventInfo.getEnddate())>24){
						txtEventFrom.setText(AppConstant.dateConvert(AppConstant.mGeneralEventInfo.getStartdate())+" - "+AppConstant.dateConvertWithYear(AppConstant.mGeneralEventInfo.getEnddate()));		
					}else{
						txtEventFrom.setText(""+AppConstant.dateConvertWithYear(AppConstant.mGeneralEventInfo.getStartdate()));		
					}
					
				}
				
				txtEventName.setText(AppConstant.mGeneralEventInfo.getName().toUpperCase(Locale.getDefault()));
				txtEventDetail.setText(AppConstant.mGeneralEventInfo.getDescription().replaceAll("\\<[^>]*>",""));
				 tempPhone=new ArrayList<String>();
				if(tempPhone!=null){
					tempPhone.clear();
				}
				if(!TextUtils.isEmpty(AppConstant.mGeneralEventInfo.getPhoneStr())){
					tempPhone= Arrays.asList(AppConstant.mGeneralEventInfo.getPhoneStr().split("\\s*,\\s*"));
					btnCallEvnet.setText("Call: " +tempPhone.get(0));
					btnCallEvnet.setVisibility(View.VISIBLE);
				}else{
					btnCallEvnet.setVisibility(View.GONE);
				}
				
				btnCloseEventDetail.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						finish();
					}
				});
		// for Facebook Share =======================
		FacebookSdk.sdkInitialize(this);
		callbackManager = CallbackManager.Factory.create();
		shareDialog = new ShareDialog(this);

		btnShareOnFb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ShareDialog.canShow(ShareLinkContent.class)) {
					ShareLinkContent linkContent = new ShareLinkContent.Builder()
							.setContentTitle(AppConstant.mGeneralEventInfo.getName())
							.setContentUrl(Uri.parse("http://www.bahamas.com"))
							.build();
					shareDialog.show(linkContent);
				}

			}
		});
				
			}
	
	public void callEvnet(View v){
			if(tempPhone.size()>0){
				  Intent call = new Intent(Intent.ACTION_DIAL);
				  call.setData(Uri.parse("tel:" + tempPhone.get(0)));
				  startActivity(call);
			}
	}
	
	public void shareEventOnFB(View v){
		Toast.makeText(context, "Share evnet " + AppConstant.mGeneralEventInfo.getName() + " On Facebook", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}
}
