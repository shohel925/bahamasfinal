package com.theislandofthebahamas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.theislandofthebahamas.datamodel.TransportTableData;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.PersistData;

import java.util.ArrayList;
import java.util.List;

public class TransportActivity extends Activity {
	
	List<TransportTableData> transportDataListOne,transportDataListTwo;
	Context con,context;
	private DatabaseHandler db;
	Typeface tf,gotham ;
	TextView tvTitle,tv_gettingHere_listTag1,tv_gettingHere_listTag2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_transport);
		con = context = this;
		AnalyticsTracker.sendTrackData(this, "Transportation", "");
		AnalyticsTracker.sendEventData(this, "Transportation" , "", "");
		tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
		tvTitle = (TextView) findViewById(R.id.tv_appName_transport);
		tvTitle.setTypeface(gotham);
		tv_gettingHere_listTag1=(TextView)findViewById(R.id.tv_gettingHere_listTag1);
		tv_gettingHere_listTag1.setTypeface(gotham);
		tv_gettingHere_listTag2=(TextView)findViewById(R.id.tv_gettingHere_listTag2);
		tv_gettingHere_listTag2.setTypeface(gotham);
		//Initializing ListView
		ListView lvOne = (ListView) findViewById(R.id.lv_gettingHere_list1);
		ListView lvTwo = (ListView) findViewById(R.id.lv_gettingHere_list2);
		db=new DatabaseHandler(con);
		
		transportDataListOne = new ArrayList<TransportTableData>();
		transportDataListTwo = new ArrayList<TransportTableData>();
		
		if(transportDataListOne!=null){
			transportDataListOne.clear();
		}
		if(transportDataListTwo!=null){
			transportDataListTwo.clear();
		}
		
		//populating list from database
		transportDataListOne = db.getTransportData("htgh", PersistData.getStringData(con, AppConstant.islandID));
		transportDataListTwo = db.getTransportData("htga",PersistData.getStringData(con, AppConstant.islandID));
		
	
		
		//initializing and setting the adapter to show list		
		final TransportAdapter transAdapterOne = new TransportAdapter(transportDataListOne);
		final TransportAdapter transAdapterTwo = new TransportAdapter(transportDataListTwo);
		
		
		
		lvOne.setAdapter(transAdapterOne);
		lvTwo.setAdapter(transAdapterTwo);
		
		lvOne.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
									  int position, long id) {
				  TransportTableData data = (TransportTableData) transAdapterOne.getItem(position);
				  //StartActivity.toActivity(con, TransportDetails.class);
				  Intent intent = new Intent(TransportActivity.this,TransportDetails.class);
				  
				  intent.putExtra("TransportTableData", data);
				  startActivity(intent);
			  }
			}); 
		lvTwo.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
									  int position, long id) {
				  TransportTableData data = (TransportTableData) transAdapterTwo.getItem(position);
				  //StartActivity.toActivity(con, TransportDetails.class);
				  Intent intent = new Intent(TransportActivity.this,TransportDetails.class);
				  
				  intent.putExtra("TransportTableData", data);
				  startActivity(intent);
			  }
			}); 

		
	}
	public void btBackOnClick(View v)
	{
		this.finish();
	}
	
	private class TransportAdapter extends BaseAdapter {

		List<TransportTableData> transportData;
		public TransportAdapter(List<TransportTableData> transportData) {
			this.transportData = transportData;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return transportData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return transportData.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(v == null){
				v = ((LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_transport_item, parent, false);
			}
			TransportTableData query=transportData.get(position);
			TextView txtTransportType = (TextView) v.findViewById(R.id.transport_row_iteamName);
			
			
			
			txtTransportType.setText(query.getTransport_type());

			txtTransportType.setTypeface(tf);
			
			
			return v;
		}
		
	}
}
