package com.theislandofthebahamas;//package com.theislandofthebahamas;
//
//import com.squareup.picasso.Picasso;
//import com.theislandofthebahamas.datamodel.MoreInformationDM;
//import com.theislandofthebahamas.datamodel.holder.AllMoreInfo;
//import com.theislandofthebahamas.util.AppConstant;
//
//import android.app.Activity;
//import android.content.Context;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//public class GeneralMoreInfoDetailActivity extends Activity {
//
//	ImageView imgMoreInfoPic;
//	TextView txtMoreInfoName;
//	ListView lstMoreInfoDetail;
//	
//	MoreInformationDM moreInfo;
//	Context context, con;
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_general_more_info_detail);
//		getActionBar().hide();
//		context = con = this;
//		
//				imgMoreInfoPic = (ImageView)findViewById(R.id.imgMoreInfoPic);
//				txtMoreInfoName = (TextView)findViewById(R.id.txtMoreInfoName);
//				lstMoreInfoDetail = (ListView)findViewById(R.id.lstMoreInfoDetail);
//				Picasso.with(context).load(moreInfo.getMoreInfoPicId()).into(imgMoreInfoPic);
//				txtMoreInfoName.setText(moreInfo.getMoreInfoName());
//				lstMoreInfoDetail.setAdapter(new MoreDetailadapter());
//			}
//
//	public void closeMoreInfoDetail(View v){
//		finish();
//	}
//	
//	private class MoreDetailadapter extends BaseAdapter{
//
//		@Override
//		public int getCount() {
//			// TODO Auto-generated method stub
//			return moreInfo.getMoreInfoDetailList().size();
//		}
//
//		@Override
//		public Object getItem(int position) {
//			// TODO Auto-generated method stub
//			return moreInfo.getMoreInfoDetailList().get(position);
//		}
//
//		@Override
//		public long getItemId(int position) {
//			// TODO Auto-generated method stub
//			return position;
//		}
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			// TODO Auto-generated method stub
//			
//			if(convertView == null){
//				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_more_info_detail, parent, false);
//			}
//			
//			TextView txtMoreInfoDetailTitle = (TextView)convertView.findViewById(R.id.txtMoreInfoDetailTitle);
//			TextView txtMoreInfoDetailDesc = (TextView)convertView.findViewById(R.id.txtMoreInfoDetailDesc);
//			txtMoreInfoDetailTitle.setText(moreInfo.getMoreInfoDetailList().get(position).getMoreInfoTitle());
//			txtMoreInfoDetailDesc.setText(moreInfo.getMoreInfoDetailList().get(position).getMoreInfoDescription());
//			
//			return convertView;
//		}
//		
//	}
//	
//}
