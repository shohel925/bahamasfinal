package com.theislandofthebahamas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theislandofthebahamas.response.ReportResponse;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.NetInfo;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.protocol.HTTP;

public class ReportActivity extends Activity {

	Context context, con;
	EditText editTextSubject,editTextDescription;
	String subject="",description="";
	Button buttonCros;
	TextView textviewReport;
	Typeface medium,gotham,demiBold;
	TextView textviewTv,textviewSubmit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.report);
		context = con = this;
		initUI();

	}
	
	private void initUI() {
		// TODO Auto-generated method stub
		textviewSubmit=(TextView)findViewById(R.id.textviewSubmit);
		textviewTv=(TextView)findViewById(R.id.textviewTv);
		textviewReport=(TextView)findViewById(R.id.textviewReport);
		buttonCros=(Button)findViewById(R.id.buttonCros);
		editTextSubject=(EditText)findViewById(R.id.editTextSubject);
		editTextDescription=(EditText)findViewById(R.id.editTextDescription);
		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		textviewReport.setTypeface(gotham);
		editTextSubject.setTypeface(medium);
		editTextDescription.setTypeface(medium);
		textviewTv.setTypeface(medium);
		editTextDescription.setTypeface(medium);
		buttonCros.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}
	public void cancelBtn(View v){
		finish();
	}
	
	public void submitBtn(View v){
		if(TextUtils.isEmpty(editTextSubject.getText().toString())){
		AlertMessage.showMessage(con, getString(R.string.app_name), getString(R.string.PleaseEnteryourSubject));
		}else if(TextUtils.isEmpty(editTextSubject.getText().toString())){
		AlertMessage.showMessage(con, getString(R.string.app_name), getString(R.string.PleaseEnteryourDescription));
		}else{
			subject=editTextSubject.getText().toString();
			description=editTextDescription.getText().toString();
			submitReport(AllURL.getMissingInfo());
		}
	}
	 protected void submitReport(final String url) {
	 
	 if (!NetInfo.isOnline(con)) {
		AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
			return;
		}

       Log.e("URL : ", url);

      final BusyDialog busyNow = new BusyDialog(con, true,false);
       busyNow.show();
       RequestParams params = new RequestParams();
       params.put("objectId", AppConstant.islandOrDirectoryId);
       params.put("subject", subject);
       params.put("reportdescription", description);
       params.put("senderemail", "chepple@exploramobile.com");
       params.put("objectType", AppConstant.objectType);

       final AsyncHttpClient client = new AsyncHttpClient();
       client.addHeader(HTTP.CONTENT_TYPE,"application/x-www-form-urlencoded;charset=UTF-8");
       client.post(url,params, new AsyncHttpResponseHandler() {

           @Override
           public void onStart() {
               // called before request is started
           }

		   @Override
		   public void onSuccess(int statusCode, Header[] headers, byte[] response) {

			   if (busyNow != null) {
				   busyNow.dismis();
			   }

			   try {
				   Log.e("Response", ">>" + new String(response));
				   Gson g = new Gson();
				   ReportResponse mReportResponse = g.fromJson(new String(response),ReportResponse.class);

				   if (mReportResponse.getStatus().equalsIgnoreCase("true")) {

					   Toast.makeText(con,getString(R.string.success), Toast.LENGTH_SHORT).show();
					   finish();


				   } else {
					   AlertMessage.showMessage(con, getString(R.string.app_name), mReportResponse.getMessage() + "");

				   }


			   } catch (final Exception e) {

				   e.printStackTrace();
			   }

		   }

           @Override
           public void onFailure(int statusCode, Header[] headers,
                                 byte[] errorResponse, Throwable e) {
               // called when response HTTP status is "4XX" (eg. 401, 403, 404)

               if (busyNow != null) {
                   busyNow.dismis();
               }
           }

           @Override
           public void onRetry(int retryNo) {
               // called when request is retried
           }


		
       });
   }

}
