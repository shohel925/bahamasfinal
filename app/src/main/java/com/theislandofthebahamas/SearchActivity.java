package com.theislandofthebahamas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.ReportResponse;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.StartActivity;

import java.util.Vector;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.protocol.HTTP;

public class SearchActivity extends Activity {

	Context context, con;
	Typeface medium,gotham,demiBold;
	TextView buttoncancel;
	DatabaseHandler db;
	ListView listviewSearch;
	Vector<DivingDirectoryInfo> tempDirectory;
	EditText exittextSearch;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.search);
		context = con = this;
		db=new DatabaseHandler(con);
		initUI();

	}
	
	private void initUI() {
		// TODO Auto-generated method stub
		exittextSearch=(EditText)findViewById(R.id.exittextSearch);
		listviewSearch=(ListView)findViewById(R.id.listviewSearch);
		buttoncancel=(TextView)findViewById(R.id.buttoncancel);
		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		buttoncancel.setTypeface(demiBold);
		buttoncancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		exittextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if(TextUtils.isEmpty(exittextSearch.getText().toString())){
						Toast.makeText(con,getString(R.string.PleaseEnterSearch),Toast.LENGTH_LONG).show();
					}else{
						if(tempDirectory!=null){
							tempDirectory.clear();
						}
						tempDirectory=db.getDivingDirFromFromTitle(exittextSearch.getText().toString());
						listviewSearch.setAdapter(new DivingDirAdapter());
					}
					return true;
				}
				return false;
			}
		});

		exittextSearch.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {

				// you can call or do what you want with your EditText here



			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(tempDirectory!=null){
					tempDirectory.clear();
				}
				tempDirectory=db.getDivingDirFromFromTitle(s.toString());
				listviewSearch.setAdapter(new DivingDirAdapter());
			}
		});
		tempDirectory=new Vector<>();
		if(tempDirectory!=null){
			tempDirectory.clear();
		}
		tempDirectory=db.getDivingDirFromDB();
		listviewSearch.setAdapter(new DivingDirAdapter());
		
	}

	private class DivingDirAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tempDirectory.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tempDirectory.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_diving_dir_detail, parent, false);
			}
			DivingDirectoryInfo query=tempDirectory.get(position);
			ImageView imgDivingDirPic = (ImageView) convertView.findViewById(R.id.imgDivingDetaisRow);
			TextView txtDivingDirName = (TextView) convertView.findViewById(R.id.tv_diving_dir_name);
			TextView txtDivingDirLocation = (TextView) convertView.findViewById(R.id.tv_diving_dir_location);
			TextView txtDivingDirPhone = (TextView) convertView.findViewById(R.id.tv_diving_dir_phone);

			Picasso.with(context).load(query.getImage_url()).placeholder(R.drawable.temp).into(imgDivingDirPic);
			txtDivingDirName.setText(query.getName());
			txtDivingDirLocation.setText(query.getArea());
			if((query.getPhone().trim().equalsIgnoreCase("false"))||(query.getPhone().trim().equalsIgnoreCase("-"))){
				txtDivingDirPhone.setVisibility(View.GONE);
			}else{
				txtDivingDirPhone.setText(query.getPhone());
				txtDivingDirPhone.setVisibility(View.VISIBLE);
			}

			txtDivingDirName.setTypeface(medium);
			txtDivingDirLocation.setTypeface(medium);
			txtDivingDirPhone.setTypeface(medium);
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					AppConstant.mDivingDirectoryInfo=tempDirectory.get(position);
					StartActivity.toActivity(con, SearchDetailActivity.class);

				}
			});

			return convertView;
		}

	}


}
