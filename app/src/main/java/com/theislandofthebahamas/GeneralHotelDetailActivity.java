package com.theislandofthebahamas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BaseURL;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

public class GeneralHotelDetailActivity extends Activity {

	
	ImageView imgHotelPic;
	TextView txtHotelAddress;
	TextView txtHotelAddressDetail;
	TextView txtHotelAddressCountry;
	Button btnCallHotel,btnHotelShareOnFB;
	TextView btnHotelOnThisIsland;
	ImageView imgMapHolder;
	Context context, con;
	Typeface medium,gotham,demiBold;
	ShareDialog shareDialog;
	CallbackManager callbackManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_general_hotel_detail);
		context = con = this;
		updateCustomActionbar();
		// for Facebook Share =======================
		FacebookSdk.sdkInitialize(this);
		callbackManager = CallbackManager.Factory.create();
		shareDialog = new ShareDialog(this);

		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		btnHotelShareOnFB=(Button)findViewById(R.id.btnHotelShareOnFB);
		btnHotelOnThisIsland=(TextView)findViewById(R.id.btnHotelOnThisIsland);
				imgHotelPic = (ImageView)findViewById(R.id.imgHotelPic);
				txtHotelAddress = (TextView)findViewById(R.id.txtHotelAddress);
				txtHotelAddressDetail = (TextView)findViewById(R.id.txtHotelAddressDetail);
				txtHotelAddressCountry = (TextView)findViewById(R.id.txtHotelAddressCountry);
				btnCallHotel = (Button) findViewById(R.id.btnCallHotel);
				imgMapHolder = (ImageView) findViewById(R.id.imgMapHolder);
				txtHotelAddress.setTypeface(medium);
				txtHotelAddressDetail.setTypeface(medium);
				btnCallHotel.setTypeface(demiBold);
				btnHotelShareOnFB.setTypeface(demiBold);
				btnHotelOnThisIsland.setTypeface(medium);
				if(!TextUtils.isEmpty(AppConstant.mGeneralHotelInfo.getImage_url())){
					imgHotelPic.setVisibility(View.VISIBLE);
					Picasso.with(context).load(AppConstant.mGeneralHotelInfo.getImage_url()).into(imgHotelPic);
				}else{
					imgHotelPic.setVisibility(View.GONE);
				}
				if(!TextUtils.isEmpty(AppConstant.mGeneralHotelInfo.getPhone())){
					btnCallHotel.setVisibility(View.VISIBLE);
					btnCallHotel.setText("CALL: " + AppConstant.mGeneralHotelInfo.getPhone());
				}else{
					btnCallHotel.setVisibility(View.GONE);
				}
				
				txtHotelAddress.setText(AppConstant.mGeneralHotelInfo.getName());
				txtHotelAddressDetail.setText(AppConstant.mGeneralHotelInfo.getAddress());
				
				//Need optimization of the api call
//				if(!AppConstant.mGeneralHotelInfo.getLat().equals("false")){
//					imgMapHolder.setVisibility(View.VISIBLE);
//					String mapUrl = BaseURL.BASE_URL_STATIC_MAP + AppConstant.mGeneralHotelInfo.getLat() + "," + AppConstant.mGeneralHotelInfo.getLon();
//					Picasso.with(context).load(mapUrl).into(imgMapHolder);
//				}else{
//					imgMapHolder.setVisibility(View.GONE);
//				}
				
				btnCallHotel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						if(!TextUtils.isEmpty(AppConstant.mGeneralHotelInfo.getPhone())){
							  Intent call = new Intent(Intent.ACTION_DIAL);
							  call.setData(Uri.parse("tel:" + AppConstant.mGeneralHotelInfo.getPhone()));
							  startActivity(call);
						}
						
					}
				});

		btnHotelOnThisIsland.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				PersistData.setStringData(con, AppConstant.islandID, AppConstant.mGeneralHotelInfo.getIslandid());
				StartActivity.toActivity(con, FinalHotelActivity.class);
				finish();
				if(FinalHotelActivity.getInstance()!=null){
					FinalHotelActivity.getInstance().finish();
				}

			}
		});
		
	}
	
	private void updateCustomActionbar(){
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.ab_hotel_detail);
		
		TextView btnCloseHotelDetail = (TextView)actionBar.getCustomView().findViewById(R.id.btnCloseHotelDetail);
		Button btnEditHotelInfo = (Button)actionBar.getCustomView().findViewById(R.id.btnEditHotelInfo);
		TextView txtHotelName = (TextView)actionBar.getCustomView().findViewById(R.id.txtHotelName);
		txtHotelName.setTypeface(gotham);
		btnEditHotelInfo.setTypeface(gotham);
		
		try{
			txtHotelName.setText(AppConstant.mGeneralHotelInfo.getName().toUpperCase());
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		btnCloseHotelDetail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		btnEditHotelInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.islandOrDirectoryId=AppConstant.mGeneralHotelInfo.getId();
				AppConstant.objectType="hotel";
				StartActivity.toActivity(con, ReportActivity.class);
			}
		});
		
	}
	
	public void visitHotelWebsite(View v){
		//Load web address
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://google.com"));
		startActivity(intent);
	}
	
	public void shareHotelOnFB(View v){
		if (ShareDialog.canShow(ShareLinkContent.class)) {
			ShareLinkContent linkContent = new ShareLinkContent.Builder()
					.setContentTitle(AppConstant.mGeneralHotelInfo.getName())
					.setContentDescription(AppConstant.mGeneralHotelInfo.getAddress())
					.setContentUrl(Uri.parse(AppConstant.mGeneralHotelInfo.getImage_url()))
					.build();

			shareDialog.show(linkContent);
		}
	}
	
}
