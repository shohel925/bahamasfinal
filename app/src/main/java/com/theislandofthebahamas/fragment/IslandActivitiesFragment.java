package com.theislandofthebahamas.fragment;

import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.DivingDirListActivity;
import com.theislandofthebahamas.FeatureIslandDetailActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.CategoryInfo;
import com.theislandofthebahamas.response.DivingDirectoryResponse;
import com.theislandofthebahamas.response.FilterRespone;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.util.AAPBDHttpClient;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.BusyDialog;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Executors;

public class IslandActivitiesFragment extends Fragment {

	GridView gvActivities;
	
	Vector<GeneralActivityList> activityList,tempList;
	Context context, con;
	DatabaseHandler db;
	List<Drawable> imgList;
	View view;
	DivingDirectoryResponse mDivingDirectoryResponse;
	Vector<GeneralActivityIslandInfo> getFeactureIslandList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 view = inflater.inflate(R.layout.fragment_general_activities, container, false);
		context = con = getActivity();
		db=new DatabaseHandler(con);
		intiUi();
		
		return view;
	}
	
	private void intiUi() {
		// TODO Auto-generated method stub
		
		if(activityList!=null){
			activityList.removeAllElements();
		}
		activityList=db.getAllGeneralActivity();
		gvActivities = (GridView) view.findViewById(R.id.generalGridView);
		
		tempList=new Vector<GeneralActivityList>();
		if(tempList!=null){
			tempList.removeAllElements();
		}
		
		for(int i=0; i<activityList.size(); i++){
			if(activityList.get(i).getId().equalsIgnoreCase("52")){
				
			}else{
				if(activityList.get(i).getDescription().length()>0){
					tempList.add(activityList.get(i));
				}
			}
		}
		imgList=new ArrayList<Drawable>();
		if(imgList!=null){
			imgList.clear();
		}
		imgList=AppConstant.getActivityImage(con);
		gvActivities.setAdapter(new ActivityAdapter());
		gvActivities.setOnItemClickListener(generalActivityClick);
		
	}

	private class ActivityAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tempList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tempList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_activity_list, parent, false);
			}
			
			ImageView imgActivityPic = (ImageView) convertView.findViewById(R.id.imgActivityPic);
			TextView txtActivityName = (TextView) convertView.findViewById(R.id.txtActivityName);
//			Picasso.with(context).load(imgList.get(position)).into(imgActivityPic);
			imgActivityPic.setImageDrawable(imgList.get(position));
			txtActivityName.setText(tempList.get(position).getName().toUpperCase());
			
			return convertView;
		}
		
	}
	
	private OnItemClickListener generalActivityClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
//			Toast.makeText(con, "Not Implemented", Toast.LENGTH_LONG).show();
			GeneralActivityList query=tempList.get(position);
			AppConstant.mGeneralActivityList=query;
			AppConstant.isFromIsland=true;
			getFeactureIslandList=new Vector<GeneralActivityIslandInfo>();
			getFeactureIslandList=db.getFeactureListFromActivityAndIslandId(query.getId(), PersistData.getStringData(con, AppConstant.islandID));
			if(getFeactureIslandList.size()>0){
				AppConstant.mGeneralActivityIslandInfo=getFeactureIslandList.get(0);
				StartActivity.toActivity(con, FeatureIslandDetailActivity.class);
			}else{
				StartActivity.toActivity(con, DivingDirListActivity.class);
				
			}
			
		}
	};

	
}
