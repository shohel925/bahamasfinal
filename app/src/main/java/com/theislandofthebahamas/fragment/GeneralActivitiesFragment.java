package com.theislandofthebahamas.fragment;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.GeneralActivityDetailActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.StartActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class GeneralActivitiesFragment extends Fragment {

	GridView gvActivities;
	
	Vector<GeneralActivityList> activityList,tempList;
	List<Drawable> imgList;
	Context context, con;
	DatabaseHandler db;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_general_activities, container, false);
		context = con = getActivity();
		db=new DatabaseHandler(con);
		if(activityList!=null){
			activityList.removeAllElements();
		}
		tempList=new Vector<GeneralActivityList>();
		if(tempList!=null){
			tempList.removeAllElements();
		}
		imgList=new ArrayList<Drawable>();
		if(imgList!=null){
			imgList.clear();
		}
		imgList=AppConstant.getActivityImage(con);
		activityList=db.getAllGeneralActivity();
		for(int i=0; i<activityList.size(); i++){
			if(activityList.get(i).getId().equalsIgnoreCase("52")){
				
			}else{
				if(activityList.get(i).getDescription().length()>0){
					tempList.add(activityList.get(i));
				}
			}
		}
		gvActivities = (GridView) v.findViewById(R.id.generalGridView);
		gvActivities.setAdapter(new ActivityAdapter());
		gvActivities.setOnItemClickListener(generalActivityClick);
		
		return v;
//		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	private class ActivityAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tempList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tempList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_activity_list, parent, false);
			}
			
			ImageView imgActivityPic = (ImageView) convertView.findViewById(R.id.imgActivityPic);
			TextView txtActivityName = (TextView) convertView.findViewById(R.id.txtActivityName);
			try {
				GeneralActivityList query=tempList.get(position);
				
				Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
//				Picasso.with(context).load(imgList.get(position)).into(imgActivityPic);
				imgActivityPic.setImageDrawable(imgList.get(position));
				txtActivityName.setText(query.getName().toUpperCase());
				txtActivityName.setTypeface(tf);
				
			}
			catch (Exception e) {
				// TODO: handle exception
				e.getMessage();
			}
			
			return convertView;
		}
		
	}
	
	private OnItemClickListener generalActivityClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			GeneralActivityList query=tempList.get(position);
			AppConstant.mGeneralActivityList=query;
			AppConstant.isFromIsland=false;
			StartActivity.toActivity(con, GeneralActivityDetailActivity.class);
			
			
		}
	};
	
}
