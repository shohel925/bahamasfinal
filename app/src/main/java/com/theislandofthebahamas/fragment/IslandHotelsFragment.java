package com.theislandofthebahamas.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.GeneralHotelDetailActivity;
import com.theislandofthebahamas.IslandMainActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.datamodel.TempIslandInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralHotelResponse;
import com.theislandofthebahamas.util.AAPBDHttpClient;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Executors;

public class IslandHotelsFragment extends Fragment {


	ListView listviewIslandHotel;
	
	Vector<GeneralHotelInfo> hotelList;
	
	Context context, con;
	DatabaseHandler db;
	MapView islandHotelMap;
	GoogleMap hGoogleMap;
	private boolean mapsSupported = true;
	View v;
	ProgressBar progressHotel;
	Typeface medium,gothamBold;
	TextView textviewIslandLeftName,textviewIslandNameMiddle,textviewIslandNameRight;
	int pos=0;
	List<TempIslandInfo> tempList;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 v = inflater.inflate(R.layout.fragment_island_hotels, container, false);
		context = con = getActivity();
		db=new DatabaseHandler(con);
		initUi();
		
		return v;
	}
	private void initUi() {
		// TODO Auto-generated method stub

		tempList=new ArrayList<>();
		if(tempList!=null){
			tempList.clear();
		}
		tempList=AppConstant.tempIslandList(con);
		medium = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gothamBold = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
		progressHotel=(ProgressBar)v.findViewById(R.id.progressHotel);
		listviewIslandHotel = (ListView)v.findViewById(R.id.listviewIslandHotel);
		islandHotelMap = (MapView) v.findViewById(R.id.islandHotelMap);
		textviewIslandLeftName=(TextView)v.findViewById(R.id.textviewIslandLeftName);
		textviewIslandNameMiddle=(TextView)v.findViewById(R.id.textviewIslandNameMiddle);
		textviewIslandNameRight=(TextView)v.findViewById(R.id.textviewIslandNameRight);
		textviewIslandNameRight.setTypeface(medium);
		textviewIslandLeftName.setTypeface(medium);
		textviewIslandNameMiddle.setTypeface(medium);

		pos=AppConstant.islandPos;
		setIslandData();

//		if(hotelList!=null){
//			hotelList.removeAllElements();
//		}
//		hotelList=db.getIslandHotelListFromIslandId(tempList.get(pos).getId());
//		if(hotelList.size()>0){
//			listviewIslandHotel.setAdapter(new HotelAdapter());
//		}

		IslandMainActivity.getInstance().islandMap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(listviewIslandHotel.getVisibility()== View.VISIBLE){
					IslandMainActivity.getInstance().islandMap.setImageResource(R.drawable.island_hotel_directions_map_06);;
					listviewIslandHotel.setVisibility(View.GONE);
					islandHotelMap.setVisibility(View.VISIBLE);
					Handler handler=new Handler();
					handler.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							loadMapData();	
						}
					}, 1000);
					
				}else{
					IslandMainActivity.getInstance().islandMap.setImageResource(R.drawable.island_hotel_directions_map_06);;
					listviewIslandHotel.setVisibility(View.VISIBLE);
					islandHotelMap.setVisibility(View.GONE);
				}
				
			}
		});
		textviewIslandLeftName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				if(pos==0){
					pos=tempList.size()-1;
				}else{
					pos--;
				}
				setIslandData();
			}
		});
//		textviewIslandNameMiddle.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View view) {
//
//			}
//		});
		textviewIslandNameRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				if(pos==(tempList.size()-1)){
					pos=0;
				}else{
					pos++;
				}
				setIslandData();
			}
		});
	}

	private void setIslandData() {
		if(hotelList!=null){
			hotelList.removeAllElements();
		}
		hotelList=db.getIslandHotelListFromIslandId(tempList.get(pos).getId());
		if(hotelList.size()>0){
			listviewIslandHotel.setAdapter(new HotelAdapter());
		}
		textviewIslandNameMiddle.setText(tempList.get(pos).getName());
		if(pos<1){
			textviewIslandLeftName.setText(tempList.get(15).getName());
		}else{
			textviewIslandLeftName.setText(tempList.get(pos-1).getName());
		}
		if(pos<15){
			textviewIslandNameRight.setText(tempList.get(pos+1).getName());
		}else{
			textviewIslandNameRight.setText(tempList.get(0).getName());
		}

	}

	private void initializeMap() {
	        if (hGoogleMap == null && mapsSupported) {
	        	islandHotelMap = (MapView) v.findViewById(R.id.islandHotelMap);
	            hGoogleMap = islandHotelMap.getMap();
	            //setup markers etc...
	        }
	    }
	
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			
			MapsInitializer.initialize(getActivity());
		
		    if (islandHotelMap != null) {
		    	islandHotelMap.onCreate(savedInstanceState);
		    }
		    initializeMap();
		}
	
	
	private void loadMapData() {
		if(hGoogleMap!=null){
			hGoogleMap.clear();
		}
		Log.e("map size", ">>"+hotelList.size());
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (int i = 0; i < hotelList.size(); i++) {
			if(((!hotelList.get(i).getLat().trim().equalsIgnoreCase("false"))&&(!hotelList.get(i).getLon().trim().equalsIgnoreCase("false")))){
				Log.e("map true", ">>"+hotelList.size());
				LatLng currentPosition = new LatLng(Double.parseDouble(hotelList.get(i).getLat()), Double.parseDouble(hotelList.get(i).getLon()));
				hGoogleMap.addMarker(new MarkerOptions().position(currentPosition).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
						.title(hotelList.get(i).getName()));
				builder.include(currentPosition);	
			}
		}
		try {
				LatLngBounds bounds = builder.build();
				int padding = 100;
				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
				hGoogleMap.animateCamera(cu);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	 @Override
	    public void onResume() {
	        super.onResume();
	        islandHotelMap.onResume();
	        initializeMap();
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        islandHotelMap.onPause();
	    }

	    @Override
	    public void onDestroy() {
	        super.onDestroy();
	        islandHotelMap.onDestroy();
	    }

	    @Override
	    public void onLowMemory() {
	        super.onLowMemory();
	        islandHotelMap.onLowMemory();
	    }
	    
	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        islandHotelMap.onSaveInstanceState(outState);
	    }


	private class HotelAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return hotelList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return hotelList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				convertView = ((LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.island_hotel_rows, parent, false);
			}

			GeneralHotelInfo query=hotelList.get(position);
			ImageView imageViewIslandHotelRows = (ImageView) convertView.findViewById(R.id.imageViewIslandHotelRows);
			TextView textViewIslandHotelTitle = (TextView) convertView.findViewById(R.id.textViewIslandHotelTitle);
			TextView textviewIslandAddress = (TextView) convertView.findViewById(R.id.textviewIslandAddress);
			TextView textviewIslandName=(TextView)convertView.findViewById(R.id.textviewIslandName);
			TextView hotelPHone=(TextView)convertView.findViewById(R.id.hotelPHone);
			textviewIslandAddress.setTypeface(medium);
			textViewIslandHotelTitle.setTypeface(gothamBold);
			textviewIslandName.setTypeface(medium);
			hotelPHone.setTypeface(medium);
			Picasso.with(con).load(query.getImage_url()).into(imageViewIslandHotelRows);
			textViewIslandHotelTitle.setText(query.getName());
			textviewIslandAddress.setText(query.getAddress().replaceAll("\\<[^>]*>",""));
			hotelPHone.setText(query.getPhone());
			textviewIslandName.setText(AppConstant.mIslandList.getName());
			textviewIslandAddress.setVisibility(View.GONE);
			if(TextUtils.isEmpty(query.getName())){
				textViewIslandHotelTitle.setVisibility(View.GONE);
			}else{
				textViewIslandHotelTitle.setVisibility(View.VISIBLE);
			}
//			if(TextUtils.isEmpty(query.getAddress())){
//				textviewIslandAddress.setVisibility(View.GONE);
//			}else{
//				textviewIslandAddress.setVisibility(View.VISIBLE);
//			}
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					GeneralHotelInfo query=hotelList.get(position);
					AppConstant.mGeneralHotelInfo=query;
					StartActivity.toActivity(con,GeneralHotelDetailActivity.class);
				}
			});

			return convertView;
		}

	}
}
