package com.theislandofthebahamas.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.FeatureIslandDetailActivity;
import com.theislandofthebahamas.FinalHotelActivity;
import com.theislandofthebahamas.GeneralHotelDetailActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralHotelResponse;
import com.theislandofthebahamas.util.AAPBDHttpClient;
import com.theislandofthebahamas.util.AlertMessage;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.Helper;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.util.Collections;
import java.util.Vector;
import java.util.concurrent.Executors;

public class GeneralHotelsFragment extends Fragment {

//ListView lstHotelList;
	
	Vector<GeneralHotelInfo> hotelList;
	
	Context context, con;
	TextView textViewHotel;
	Typeface tf,gothamBold;
	DatabaseHandler db;
	ProgressBar progressHotel;
	LinearLayout genealHotelHolder;
	TextView OtherHotel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_general_hotels, container, false);
		context = con = getActivity();
		 db=new DatabaseHandler(con);
		 tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		 gothamBold = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
		 if(hotelList!=null){
			 hotelList.removeAllElements();
		 }
		OtherHotel=(TextView) v.findViewById(R.id.OtherHotel);
		genealHotelHolder=(LinearLayout)v.findViewById(R.id.genealHotelHolder);
		progressHotel=(ProgressBar)v.findViewById(R.id.progressHotel);
		textViewHotel=(TextView)v.findViewById(R.id.textViewHotel);
		textViewHotel.setTypeface(tf);
		hotelList=db.getHotelListFromFeature("true");
		Collections.shuffle(hotelList);
		if(hotelList.size()>0){
			loadFeatureIslands();
		}
		OtherHotel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				AppConstant.isViewDirectoryOfHotel=true;
				StartActivity.toActivity(con, FinalHotelActivity.class);
			}
		});
		
		return v;
	}

	View customView;
	int i;
	private void loadFeatureIslands() {
		// TODO Auto-generated method stub
		genealHotelHolder.removeAllViews();
		for(i = 0; i< hotelList.size(); i++){
			customView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_hotel_list, genealHotelHolder, false);
			GeneralHotelInfo query=hotelList.get(i);
			ImageView imgHotelPic = (ImageView) customView.findViewById(R.id.imgHotelPic);
			TextView txtHotelName = (TextView) customView.findViewById(R.id.txtHotelName);
			TextView txtHotelAddress = (TextView) customView.findViewById(R.id.txtHotelAddress);


			Picasso.with(context).load(query.getImage_url()).into(imgHotelPic);
			txtHotelName.setText(query.getName());
			txtHotelAddress.setText(query.getAddress());
			txtHotelName.setTypeface(gothamBold);
			txtHotelAddress.setTypeface(tf);
			customView.setTag(i);
			customView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					Log.e("ilandId", ">>"+tempIslandInfo.get(i).getIsland_id());
					Object tag =v.getTag();
					if(tag!=null) {
						int position = (Integer)tag;
						GeneralHotelInfo query=hotelList.get(position);
						AppConstant.mGeneralHotelInfo=query;
						StartActivity.toActivity(con,GeneralHotelDetailActivity.class);
					}
				}
			});

			genealHotelHolder.addView(customView);

		}


	}
	
	
	private class HotelAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return hotelList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return hotelList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_hotel_list, parent, false);
			}
			GeneralHotelInfo query=hotelList.get(position);
			ImageView imgHotelPic = (ImageView) convertView.findViewById(R.id.imgHotelPic);
			TextView txtHotelName = (TextView) convertView.findViewById(R.id.txtHotelName);
			TextView txtHotelAddress = (TextView) convertView.findViewById(R.id.txtHotelAddress);
			
			
			Picasso.with(context).load(query.getImage_url()).into(imgHotelPic);
			txtHotelName.setText(query.getName());
			txtHotelAddress.setText(query.getAddress());
			txtHotelName.setTypeface(gothamBold);
			txtHotelAddress.setTypeface(tf);
			
			return convertView;
		}
		
	}
	
	private OnItemClickListener hotelClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			GeneralHotelInfo query=hotelList.get(position);
			AppConstant.mGeneralHotelInfo=query;
			Intent intent = new Intent(context, GeneralHotelDetailActivity.class);
			startActivity(intent);
		}
	};
}
