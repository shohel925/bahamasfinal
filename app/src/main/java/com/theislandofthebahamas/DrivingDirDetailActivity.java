package com.theislandofthebahamas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.util.AnalyticsTracker;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.NetInfo;
import com.theislandofthebahamas.util.PersistData;
import com.theislandofthebahamas.util.StartActivity;

import java.util.Arrays;
import java.util.List;

public class DrivingDirDetailActivity extends Activity {

	
	Context context, con;
	ImageView imageviewTop;
	TextView textviewArea,textviewAddressDetail,directoryDetails;
	Button buttonCall,buttonShareFB;
	List<String> finaPhone;
	Typeface medium,gotham,demiBold;
	MapFragment fm;
	GoogleMap googleMap;
	LinearLayout mapLyout;
	ShareDialog shareDialog;
	CallbackManager callbackManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driving_dir_hotel_detail);
		context = con = this;
		AnalyticsTracker.sendTrackData(this,AppConstant.mGeneralActivityList.getName()+" Directory", "");
		AnalyticsTracker.sendEventData(this, AppConstant.mGeneralActivityList.getName()+" Directory", "", "");
		updateCustomActionbar();
		initUi();	
		
	}
	
	private void initUi() {
		// TODO Auto-generated method stub
		if(finaPhone!=null){
			finaPhone.clear();
		}

		// for Facebook Share =======================
		FacebookSdk.sdkInitialize(this);
		callbackManager = CallbackManager.Factory.create();
		shareDialog = new ShareDialog(this);
		mapLyout=(LinearLayout)findViewById(R.id.mapLyout);
		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		directoryDetails=(TextView)findViewById(R.id.directoryDetails);
		buttonShareFB=(Button)findViewById(R.id.buttonShareFB);
		buttonCall=(Button)findViewById(R.id.buttonCall);
		textviewAddressDetail=(TextView)findViewById(R.id.textviewAddressDetail);
		textviewArea=(TextView)findViewById(R.id.textviewArea);
		imageviewTop=(ImageView)findViewById(R.id.imageviewTop);
		textviewArea.setTypeface(medium);
		textviewAddressDetail.setTypeface(medium);
		buttonCall.setTypeface(demiBold);
		buttonShareFB.setTypeface(demiBold);
		directoryDetails.setTypeface(medium);
		textviewArea.setText(Html.fromHtml(AppConstant.mDivingDirectoryInfo.getArea()));
		directoryDetails.setText(Html.fromHtml(AppConstant.mDivingDirectoryInfo.getDescription()));
		textviewAddressDetail.setText(Html.fromHtml(AppConstant.mDivingDirectoryInfo.getAddress()));
		Log.e("Phone no", ">>"+AppConstant.mDivingDirectoryInfo.getPhone());
		fm = (MapFragment) getFragmentManager().findFragmentById(R.id.drivingMap);
		googleMap = fm.getMap();
		if (googleMap != null) {
			googleMap.clear();
		}

		if((AppConstant.mDivingDirectoryInfo.getPhone().trim().equals("-"))||(AppConstant.mDivingDirectoryInfo.getPhone().trim().equals("false"))){
			buttonCall.setVisibility(View.GONE);
		}else{
			buttonCall.setVisibility(View.VISIBLE);
			finaPhone = Arrays.asList(AppConstant.mDivingDirectoryInfo.getPhone().split("\\s*,\\s*"));
			buttonCall.setText(getString(R.string.call)+finaPhone.get(0));
		}
		if(!TextUtils.isEmpty(AppConstant.mDivingDirectoryInfo.getImage_url())){
			imageviewTop.setVisibility(View.VISIBLE);
			Picasso.with(context).load(AppConstant.mDivingDirectoryInfo.getImage_url()).into(imageviewTop);
		}else{
			imageviewTop.setVisibility(View.GONE);
		}
		
		if((!AppConstant.mDivingDirectoryInfo.getLat().equals("false"))&&(!AppConstant.mDivingDirectoryInfo.getLon().equals("false"))){
			mapLyout.setVisibility(View.VISIBLE);
			drawMarker(Double.parseDouble(AppConstant.mDivingDirectoryInfo.getLat()), Double.parseDouble(AppConstant.mDivingDirectoryInfo.getLon()),AppConstant.mDivingDirectoryInfo.getName());
		}else{
			mapLyout.setVisibility(View.GONE);
		}
		
		buttonCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent call = new Intent(Intent.ACTION_DIAL);
				  call.setData(Uri.parse("tel:" + finaPhone.get(0)));
				  startActivity(call);
			}
		});

		googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {

				Log.e("Title", ":" + marker.getTitle());
				try {
					marker.showInfoWindow();
//					if (marker.getTitle().equalsIgnoreCase("Current Location")) {
//						AppConstant.pickup_lat = marker.getPosition().latitude;
//						AppConstant.pickup_lng = marker.getPosition().longitude;
//						getDistance(marker);
//						marker.showInfoWindow();
//					}
//					else {
//						marker.hideInfoWindow();
//					}

				}
				catch (Exception e) {
					e.printStackTrace();
				}

				return true;
			}
		});

		googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker marker) {
				// TODO Auto-generated method stub

				if(NetInfo.isOnline(con)){
					StartActivity.toActivity(con, DirectionActivity.class);
				}
			}
		});

		googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
			private View myContentsView;
			private TextView address,title;

			// Use default InfoWindow frame
			@Override
			public View getInfoWindow(Marker marker) {
				Log.e("markerTitle", "" + marker.getTitle());
				marker.getSnippet();
				myContentsView = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
				myContentsView.setDrawingCacheBackgroundColor(Color.BLACK);
				address = (TextView) myContentsView.findViewById(R.id.address);
				title = (TextView) myContentsView.findViewById(R.id.textviewTitle);
				title.setText(marker.getTitle());
				address.setText(marker.getSnippet());
				return myContentsView;
			}

			// Defines the contents of the InfoWindow
			@Override
			public View getInfoContents(Marker marker) {

				return null;

			}
		});
	}

	private void drawMarker(Double lat, Double lng, String title) {
		LatLng currentPosition = new LatLng(lat, lng);
		// AppConstant.pLatLon = currentPosition;
		googleMap.addMarker(new MarkerOptions().position(currentPosition).snippet(AppConstant.mDivingDirectoryInfo.getCity() )
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin1)).draggable(true).title(title));
		CameraUpdate center = CameraUpdateFactory.newLatLng(currentPosition);
//		CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
		googleMap.animateCamera(center);
//		googleMap.animateCamera(zoom);
//		googleMap.moveCamera(center);
	}

	private void updateCustomActionbar(){
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.ab_hotel_detail);

		TextView btnCloseHotelDetail = (TextView)actionBar.getCustomView().findViewById(R.id.btnCloseHotelDetail);
		Button btnEditHotelInfo = (Button)actionBar.getCustomView().findViewById(R.id.btnEditHotelInfo);
		TextView txtHotelName = (TextView)actionBar.getCustomView().findViewById(R.id.txtHotelName);
		
		try{
			txtHotelName.setText(AppConstant.mDivingDirectoryInfo.getName().toUpperCase());
			txtHotelName.setTypeface(gotham);
			btnEditHotelInfo.setTypeface(gotham);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		btnCloseHotelDetail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		btnEditHotelInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.islandOrDirectoryId=AppConstant.mDivingDirectoryInfo.getId();
				AppConstant.objectType="directory";
				StartActivity.toActivity(con, ReportActivity.class);
			}
		});
		
	}
	
	public void visitHotelWebsite(View v){
		//Load web address
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://google.com"));
		startActivity(intent);
	}
	
	public void shareHotelOnFB(View v){
		if (ShareDialog.canShow(ShareLinkContent.class)) {
			ShareLinkContent linkContent = new ShareLinkContent.Builder()
					.setContentTitle(AppConstant.mDivingDirectoryInfo.getName())
					.setContentDescription(AppConstant.mDivingDirectoryInfo.getAddress())
					.setContentUrl(Uri.parse(AppConstant.mDivingDirectoryInfo.getImage_url()))
					.build();

			shareDialog.show(linkContent);
		}
	}
	
	public void viewOtherHotelOnThisIsland(View v){
		PersistData.setStringData(con, AppConstant.islandID, AppConstant.mGeneralHotelInfo.getIslandid());
		StartActivity.toActivity(con, FinalHotelActivity.class);
		finish();
		if(FinalHotelActivity.getInstance()!=null){
			FinalHotelActivity.getInstance().finish();
		}
	}
	
}
